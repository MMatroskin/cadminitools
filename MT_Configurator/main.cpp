#include "mainwindow.h"
#include <QApplication>
#include <QStyleFactory>
#include <string>

#pragma comment(lib, "Advapi32.lib")
#pragma comment(lib, "User32.lib")

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
	a.setStyle(QStyleFactory::create("Fusion"));
    MainWindow w;

    w.show();

    return a.exec();
}
