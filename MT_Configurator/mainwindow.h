#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ui_mainwindow.h"
#include <QMainWindow>
#include <string>
#include <list>
#include <map>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow, public Ui_MainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    std::wstring appPath;
    std::wstring fileName;
    std::wstring sectionName;
    std::map<std::wstring, std::wstring> params;

    void SetKeys();

private slots:
    void on_pushButtonOk_clicked();
    void slotExit();

};

#endif // MAINWINDOW_H
