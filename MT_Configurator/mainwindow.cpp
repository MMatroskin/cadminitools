#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "configSrv.h"
#include "files.h"
#include "app.h"
#include <algorithm>
#include <QString>
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    appPath = App::App::GetAppPath();
    fileName = appPath + L"\\MiniTools\\Main\\MiniTools.cfg";
    sectionName = L"userInfo";
    params = Files::ConfigSrv::GetSectParams(fileName, sectionName);

    ui->setupUi(this);

    if(!params.empty()){
        //auto it = params.begin();
        map<wstring, wstring>::iterator it;
        it = params.find(L"_author1");
        if(it != params.end()){
            ui->lineEditUser->setText(QString::fromStdWString(it->second));
        }
        it = params.find(L"_inspector");
        if(it != params.end()){
            ui->lineEditInspector->setText(QString::fromStdWString(it->second));
        }
        it = params.find(L"_head_of_dept");
        if(it != params.end()){
            ui->lineEditHead->setText(QString::fromStdWString(it->second));
        }
        it = params.find(L"_standard_control");
        if(it != params.end()){
            ui->lineEditCheck->setText(QString::fromStdWString(it->second));
        }
        it = params.find(L"_project_manager");
        if(it != params.end()){
            ui->lineEditPM->setText(QString::fromStdWString(it->second));
        }
        it = params.find(L"_company");
        if(it != params.end()){
            ui->lineEditFirm->setText(QString::fromStdWString(it->second));
        }
    }

    connect(ui->pushButtonCancel, &QPushButton::clicked, this, &MainWindow::slotExit);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::slotExit(){

    this->close();
}

void MainWindow::on_pushButtonOk_clicked(){

    SetKeys();

    list<wstring> sourceList = Files::Files::GetFileLinesList(fileName);
    list<wstring> targetList = Files::ConfigSrv::InsertParamsInSect(sourceList, params, sectionName);

    bool result = Files::Files::CreateTextFile(fileName, targetList);

    QString message;
    if(result){
        message = "Данные сохранены.";
    }else{
        message = "Ошибка записи.";
    }

    QMessageBox msg;
    msg.setText(message);

    msg.exec();

    this->close();
}

void MainWindow::SetKeys(){

    map<wstring, wstring>::iterator it;
    wstring key;
    wstring value;

    key = L"_author1";
    value = ui->lineEditUser->text().toStdWString();
    it = params.find(key);
    if(it != params.end()){
        params.at(key) = value;
    }else{
        params.insert(pair<wstring, wstring>(key, value));
    }

    key = L"_inspector";
    value = ui->lineEditInspector->text().toStdWString();
    it = params.find(key);
    if(it != params.end()){
        params.at(key) = value;
    }else{
        params.insert(pair<wstring, wstring>(key, value));
    }

    key = L"_head_of_dept";
    value = ui->lineEditHead->text().toStdWString();
    it = params.find(key);
    if(it != params.end()){
        params.at(key) = value;
    }else{
        params.insert(pair<wstring, wstring>(key, value));
    }

    key = L"_standard_control";
    value = ui->lineEditCheck->text().toStdWString();
    it = params.find(key);
    if(it != params.end()){
        params.at(key) = value;
    }else{
        params.insert(pair<wstring, wstring>(key, value));
    }

    key = L"_project_manager";
    value = ui->lineEditPM->text().toStdWString();
    it = params.find(key);
    if(it != params.end()){
        params.at(key) = value;
    }else{
        params.insert(pair<wstring, wstring>(key, value));
    }

    key = L"_company";
    value = ui->lineEditFirm->text().toStdWString();
    it = params.find(key);
    if(it != params.end()){
        params.at(key) = value;
    }else{
        params.insert(pair<wstring, wstring>(key, value));
    }

}
