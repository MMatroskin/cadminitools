;; Copyrighted by Maks Tulin 2018
;;
;; ���������� ������� ��� ��������
;;

(defun c:gp-zero (/ blockName fileName layerName oldEcho oldOsmode P1  N1 *A1 A2 userclick)
;������� ������� 0

	(setq oldEcho (getvar "cmdecho"))
	(setvar "cmdecho" 0)
	(setq oldOsmode (getvar "osmode"))
	
	(vl-load-com)
	
	(setq *A1 (list "1" "2000" "1000" "500"))

	(setq dcl_id (load_dialog "MT_main.dcl"))
	(new_dialog "common_mark" dcl_id)

	(start_list "A1")
	(mapcar 'add_list *A1)
	(end_list)

	(action_tile "A1" "(setq *A2 (get_tile \"A1\"))")
	(action_tile "accept" "(setq *A2 (get_tile \"A1\")) (SETQ userclick 1) (done_dialog)")
	(action_tile "cancel" "(SETQ userclick 0) (done_dialog)")

	(start_dialog)
	(unload_dialog dcl_id)

	(if (= *A2 "") ;�� ������
		(setq A2 0.5)
		(setq A2 (/ (atof (nth (atoi *A2) *A1)) 1000))
	)
	(if (= A2 0.001)
		(setq A2 1)
	)
	
	(if (= userclick 1)
		(progn
			(setq layerName "V_LevelZero")
			(if (tblsearch "layer" layerName)
				(command "_layer" "_Set" layerName "")
				(command "_layer" "_set" "0" "_Make" layerName "_LWeight" "0.25" layerName "_Color" "7" layerName "_set" layerName "")
			)

			(setvar "celtype" "ByLayer")
			(setvar "cecolor" "256")
			(setvar "CELWEIGHT" -1)
			
			(setq blockName "GP_ZERO_DIN")
			(setq fileName "lib_MP1.dwg")
			
			(if (not (tblsearch "block" blockName))
				(MT-importBlockFromLib blockName fileName)
			)
			
			(setq P1 (getpoint "\n������� ����� ������� <�����> : "))
			(princ)
			(if (= P1 nil)
				(setq N1 1) (setq N1 0)
			)
			(while (= N1 0)
				(vl-cmdf "_.-insert" blockName P1 A2 A2 0)
				
				(setq P1 (getpoint "\n������� ����� ������� <�����> : "))
				(princ)
				(if (= P1 nil)
					(setq N1 1) (setq N1 0)
				)
				
			)
		)
		
	)
	
	(setvar "osmode" oldOsmode)
	(setvar "cmdecho" oldEcho)
	(princ "\n���������")
	(princ)
)

(defun c:gp-redpt (/ layerName oldEcho oldOsmode P1 P2 N1 *A1 A2 userclick  A B A1 A2 P1 P2 str0 str1)
;������� ������� �������

	(setq oldEcho (getvar "cmdecho"))
	(setvar "cmdecho" 0)
	(setvar "attdia" 0)
	(setq oldOsmode (getvar "osmode"))
	
	(vl-load-com)
	
	(setq *A1 (list "1" "2000" "1000" "500"))

	(setq dcl_id (load_dialog "MT_main.dcl"))
	(new_dialog "common_mark" dcl_id)

	(start_list "A1")
	(mapcar 'add_list *A1)
	(end_list)

	(action_tile "A1" "(setq *A2 (get_tile \"A1\"))")
	(action_tile "accept" "(setq *A2 (get_tile \"A1\")) (SETQ userclick 1) (done_dialog)")
	(action_tile "cancel" "(SETQ userclick 0) (done_dialog)")

	(start_dialog)
	(unload_dialog dcl_id)

	(if (= *A2 "") ;�� ������
		(setq A2 0.5)
		(setq A2 (/ (atof (nth (atoi *A2) *A1)) 1000))
	)
	(if (= A2 0.001)
		(setq A2 1)
	)
	
	(if (= userclick 1)
		(progn
			(setvar "osmode" 1015)
			(setq P1 (getpoint "\n \n������� ����� �� ������� <�����>"))
			(if (/= P1 nil)
				(setq N1 0)
				(setq N1 1)
			)
			(while (= N1 0)
				(progn
					(setvar "osmode" 0)
					(setq P2 (getpoint P1 "\n \n������� ����� ������� <���>"))
					(if (= P2 nil)
						(setq P2 (list (+ (car P1) 0.2) (car(cdr P1)) (+ (car(cdr (cdr P1))))))
					)
					(princ)
					
					(if (= (tblsearch "block" "RED_PT") nil)
							(progn
								   (setvar "osmode" 0)
								   (command "_layer" "_set" "0" "")
								   (setvar "celweight" -2)
								   (setq  NN  (ssadd))
								   (command "_AFLAGS" 20)
							 	   (command "_color" "1")
								   (command "_-attdef" "" "RED" "�������" "" "_J" "_BL" "0,0" 2.5 0)
								   (ssadd (entlast) NN)
							 	   (command "_color" "_ByBlock")
								   (command "_-attdef" "" "BLACK" "������" "" "_J" "_TL" "0,-0.75" 2.5 0)
								   (ssadd (entlast) NN)
								   (command "_Line" "0,0" "10,0" "")
								   (ssadd (entlast) NN)
								   (command "_-BLOCK" "RED_PT" "0,0" NN "")
							)
					)
					
					(setq A "")
					(setq B "")
					(setq dcl_id (load_dialog "MT_MP.dcl"))
					(new_dialog "gp_redpt" dcl_id)
					(set_tile "A1" A)
					(set_tile "B1" B)
				  
					(defun sper (/)
						  (setq AA (get_tile "A1"))
						  (setq BB (get_tile "B1"))
					)
				  
					(action_tile "accept" "(setq *A2 (get_tile \"A1\")) (sper) (SETQ userclick 1) (done_dialog)")
					(action_tile "cancel" "(SETQ userclick 0) (done_dialog)")
				  
					(start_dialog)
					(unload_dialog dcl_id)
				  
					(if (= BB "")
						(setq B 0.00)
						(setq B (atof BB))
					)
					(if (= AA "")
						(setq A B)
						(setq A (atof AA))
					)
					(princ)
				  
					(IF (= userclick 1)
							(PROGN

								(setq layerName "V_RedPoints")
								(if (tblsearch "layer" layerName)
									(command "_layer" "_Set" layerName "")
									(command "_layer" "_set" "0" "_Make" layerName "_LWeight" "0.25" layerName "_Color" "7" layerName "_set" layerName "")
								)
			
								(setvar "celtype" "ByLayer")
								(setvar "cecolor" "256")
								(setvar "CELWEIGHT" -1)
								(setvar "osmode" 0)
								
								(setq str0 (rtos A 2 2))
								(setq str1 (rtos B 2 2))
								
								(command "_DIMLDRBLK" "_dotsmall");; ���������!!!!!
								(command "_leader" P1 P2 "_Format" "_Arrow" "" "" "_block" "RED_PT" P2 A2 A2 "0" str0 str1 "" "")
								(if (> (car P1) (car P2))
									(command "_mirror" (entlast) "" P2 (list (car P2) (+ (cadr P2) 100)) "_y")
								)
								(setvar "DIMLDRBLK" ".")
							)
					)
					(setvar "osmode" 1015)
					(setq P1 (getpoint "\n \n������� ����� �� ������� <�����>"))
					(if (= P1 nil)
						(setq N1 (1+ N1))
					)
								
				);progn
			);while
		)
				
	)

	(setvar "attdia" 1)
	(setvar "osmode" oldOsmode)
	(setvar "cmdecho" oldEcho)
	(princ "\n���������")
	(princ)
)

(load (findfile "MT_main.lsp"))
