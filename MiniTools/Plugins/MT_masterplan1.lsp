;; Copyrighted by Maks Tulin 2008-2011/2018
;;
;; ���������� ������� ��� ��������
;;

(defun MT-asmi-mlStyleCreate0 (Quont val /  dxfLst topOrd Count mlDict)
;; create Mline Style
;; original: https://www.cadtutor.net/forum/topic/3557-multiline/?do=findComment&comment=28606
;; Modified Maks Tulin 2011

	(setq dxfLst 
		(list
			'(0 . "MLINESTYLE") 
			'(102 . "{ACAD_REACTORS")'(102 . "}")
			'(100 . "AcDbMlineStyle") 
			(cons 2 (strcat (vl-string-translate "." "_" (rtos ofs1)) "_" val))
			'(70 . 16)
			'(3 . "")
			'(62 . 256)
			'(51 . 1.5708)
			'(52 . 1.5708)
			(cons 71 Quont)
			'(49 . 0.0)
			'(62 . 256) 
			(cons 6  MY-LT)
			(cons 49 (/ ofs1 2))
			'(62 . 256) 
			'(6 . "BYLAYER")
			(cons 49 (- (/ ofs1 2)))
			'(62 . 256) 
			'(6 . "BYLAYER")
		)
	)

	(if (null (member (assoc 2 dxfLst) (dictsearch (namedobjdict) "ACAD_MLINESTYLE")))
		(progn
			(setq mlDict (cdr (assoc -1 (dictsearch (namedobjdict) "ACAD_MLINESTYLE"))))
			(dictadd mlDict (cdr (assoc 2 dxfLst)) (entmakex dxfLst))
		)
	)

	(strcat (vl-string-translate "." "_" (rtos ofs1)) "_" val)

); end of asmi-mlStyleCreate

(defun MT-asmi-mlStyleCreate1 (Quont ofs1 MY-LT A1 / dxfLst mlDict)
;; create Mline Style
;; original: https://www.cadtutor.net/forum/topic/3557-multiline/?do=findComment&comment=28606
;; Modified Maks Tulin 2011

	(setq dxfLst (list '(0 . "MLINESTYLE") 
                   '(102 . "{ACAD_REACTORS")'(102 . "}")
                   '(100 . "AcDbMlineStyle") 
                    (cons 2 (strcat (vl-string-translate "." "_" (rtos ofs1)) "_" A1))
                   '(70 . 0)
                   '(3 . "")
                   '(62 . 256)
                   '(51 . 1.5708)
                   '(52 . 1.5708)
                    (cons 71 Quont)
					(cons 49 (/ ofs1 2))
                   '(62 . 256) 
                    (cons 6  MY-LT)
                    (cons 49 (- (/ ofs1 2)))
                   '(62 . 256) 
                    (cons 6  MY-LT)
             )
	)

	(if (null (member (assoc 2 dxfLst) (dictsearch (namedobjdict) "ACAD_MLINESTYLE")))
		(progn
			(setq mlDict (cdr (assoc -1 (dictsearch (namedobjdict) "ACAD_MLINESTYLE"))))
			(dictadd mlDict (cdr (assoc 2 dxfLst)) (entmakex dxfLst))
		)
	)

	(strcat (vl-string-translate "." "_" (rtos ofs1)) "_" A1)

); end of asmi-mlStyleCreate

(defun c:gp-lotoc (/ MY-LT ltype-file A1 ofs1 mpl:quont mpl:just mpl:off ptOpt stPt quont layerName oldEcho)
;�����

	(vl-load-com)
	(setq oldEcho (getvar "CMDECHO"))
	(setvar "cmdecho" 0)

	(setq MY-LT "���-�����")
	(setq ltype-file "MT_MP_lines.lin")
	
	(if (null (tblsearch "Ltype" MY-LT))
        	(command "_-linetype" "_load" MY-LT ltype-file "")
	)
	
	(setq layerName "A_Outfall")
	(command "_layer" "_set" "0" "_Make" layerName "_LWeight" "0.25" layerName "_Color" "161" layerName "_set" layerName "")
    
	(setvar "celtype" "ByLayer")
	(setvar "cecolor" "256")
	(setvar "CELWEIGHT" -1)
	
	(command "_osmode" 1015)
	
	(setq ofs1 (getreal "\n\n������ (�) <�����>: "))
	(if (= ofs1 nil)
			   (setq N 1)
               (setq N 0)
	)
	(while (= N 0)
		(setq *quont 3)
		(setq A1 "Outfall")
		
		(setq mpl:quont *quont)
		(setq mpl:off 1.0)
		(setq mpl:just "Zero")
		(setq ptOpt T)

		(while (and ptOpt (/= 'LIST (type ptOpt)))
			(initget 128)
			(setq ptOpt (getpoint (strcat "\nSpecify start point: ")))
			(if (= (type ptOpt) 'STR) (setq ptOpt (strcase ptOpt)))
			(cond
				((= 'LIST (type ptOpt))
				   (setq stPt ptOpt)
				   (princ "\nSpecify next point or [Undo]: ")
				)
			)
		)
 
		(if ptOpt
			(progn
			   (setq mlName (MT-asmi-mlStyleCreate0 mpl:quont A1))
			   (if (entlast) (setq lastEnt (entlast)))
			   (setvar "cmdecho" 0)
			   (command "_.mline" 
						"_ST" mlName
						"_S" mpl:off
						"_J" (strcat "_" mpl:just)
						stPt)
			   (setvar "CMDECHO" 1)
			   (while (= 1 (getvar "CMDACTIVE"))
					  (command pause)
			   ); end while
			   (setvar "CMDECHO" 0)
			); end progn
		); end if

		(setq ofs1 (getreal "\n\n������ (�) <�����>: "))
		(if (= ofs1 nil)
			(setq N 1)
			(setq N 0)
		)
	);while
	
	(command "_regen")
	(setvar "CMDECHO" oldEcho)
	(princ)

); end

(defun c:gp-ptr (/ MY-LT ltype-file A1 ofs1 mpl:quont mpl:just mpl:off ptOpt quont layerName oldEcho)
;����������� �����

	(setq oldEcho (getvar "CMDECHO"))
	(setvar "cmdecho" 0)

	(vl-load-com)

	(setq MY-LT "JIS_02_4.0")
	(setq ltype-file "acadiso.lin")
	(if (null (tblsearch "Ltype" MY-LT))
        	(command "_-linetype" "_load" MY-LT ltype-file "")
	)

	(setq layerName "A_Pipes")
	(command "_layer" "_set" "0" "_Make" layerName "_LWeight" "0.5" layerName "_Color" "160" layerName "_set" layerName "")
    
	(setvar "celtype" MY-LT)
	(setvar "cecolor" "256")
	(setvar "CELWEIGHT" -1)
	(command "_osmode" 1015)
	
	(initget 128)
	(setq ptOpt (getpoint (strcat "\nSpecify start point <Exit>: ")))
	(if (= ptOpt nil)
		(setq N 1)
		(setq N 0)
	)
	(while (= N 0)
		(setq ofs1 0.5 *quont 2)

		(setq A1 "Pipe")
		(setq mpl:quont *quont)
		(setq mpl:off 1.0); scale
		(setq mpl:just "Zero")
		
		(setq mlName (MT-asmi-mlStyleCreate1 mpl:quont ofs1 MY-LT A1))
		(if (entlast) (setq lastEnt (entlast)))
		(setvar "cmdecho" 0)
		(command "_.mline" 
			"_ST" mlName
			"_S" mpl:off
			"_J" (strcat "_" mpl:just)
			ptOpt)
		(setvar "CMDECHO" 1)
		(while (= 1 (getvar "CMDACTIVE"))
			(command pause)
		); end while
		(setvar "CMDECHO" 0)

       		(initget 128)
		(setq ptOpt (getpoint (strcat "\nSpecify start point <Exit>: ")))
		(if (= ptOpt nil)
			(setq N 1)
		)
	);while
	
	(setvar "celtype" "ByLayer")
	(setvar "cecolor" "256")
	(setvar "CELWEIGHT" -1)
	
	(command "_regen")
	(setvar "CMDECHO" oldEcho)
	(princ)

); end

(defun c:gp-tube (/ MY-LT ltype-file A1 ofs1 mpl:quont mpl:just mpl:off ptOpt quont layerName oldEcho)
; ������

	(setq oldEcho (getvar "CMDECHO"))
	(setvar "cmdecho" 0)

	(vl-load-com)

	(setq MY-LT "JIS_02_4.0")
	(setq ltype-file "acadiso.lin")
	(if (null (tblsearch "Ltype" MY-LT))
        	(command "_-linetype" "_load" MY-LT ltype-file "")
	)

	(setq layerName "A_Tubes")
	(command "_layer" "_set" "0" "_Make" layerName "_LWeight" "0.25" layerName "_Color" "41" layerName "_set" layerName "")
    
	(setvar "celtype" MY-LT)
	(setvar "cecolor" "256")
	(setvar "CELWEIGHT" -1)
	(command "_osmode" 1015)
	
	(initget 128)
	(setq ptOpt (getpoint (strcat "\nSpecify start point <Exit>: ")))
	(if (= ptOpt nil)
		(setq N 1)
		(setq N 0)
	)
	(while (= N 0)
		(setq ofs1 1.0 *quont 2)

		(setq A1 "Pipe")
		(setq mpl:quont *quont)
		(setq mpl:off 1.0); scale
		(setq mpl:just "Zero")
		
		(setq mlName (MT-asmi-mlStyleCreate1 mpl:quont ofs1 MY-LT A1))
		(if (entlast) (setq lastEnt (entlast)))
		(setvar "cmdecho" 0)
		(command "_.mline" 
			"_ST" mlName
			"_S" mpl:off
			"_J" (strcat "_" mpl:just)
			ptOpt)
		(setvar "CMDECHO" 1)
		(while (= 1 (getvar "CMDACTIVE"))
			(command pause)
		); end while
		(setvar "CMDECHO" 0)

       		(initget 128)
		(setq ptOpt (getpoint (strcat "\nSpecify start point <Exit>: ")))
		(if (= ptOpt nil)
			(setq N 1)
		)
	);while
	
	(setvar "celtype" "ByLayer")
	(setvar "cecolor" "256")
	(setvar "CELWEIGHT" -1)
	
	(command "_regen")
	(setvar "CMDECHO" oldEcho)
	(princ)

); end

(defun c:gp-ograzhd (/ MY-LT ltype-file oldEcho layerName)
;��������� ����������

    (setq oldEcho (getvar "cmdecho"))
    (setvar "cmdecho" 0)
	
    (setq MY-LT "���-�����")
    (setq ltype-file "MT_MP_lines")
    (if (null (tblsearch "Ltype" MY-LT))
        (command "_-linetype" "_load" MY-LT ltype-file "")
    )
	
    (setq layerName "A_Fences")
    (if (tblsearch "layer" layerName)
      	(command "_layer" "_Set" layerName "")
      	(command "_layer" "_set" "0" "_Make" layerName "_LWeight" "0.7" layerName "_Color" "252" layerName "_set" layerName "")
    )
	
    (setvar "celtype" MY-LT)
    (setvar "cecolor" "256")
    (setvar "CELWEIGHT" -1)

    (command "_.pline")
    (while (= 1 (getvar "CMDACTIVE"))
        (command pause)
    ); end while
	
    (setvar "celtype" "ByLayer")
    (setvar "cmdecho" oldEcho)
    (princ)
)

(defun c:gp-otkos (/ MY-LT ltype-file layerName)
;��������� ������

    (setq oldEcho (getvar "cmdecho"))
    (setvar "cmdecho" 0)
	
    (setq MY-LT "���-�����")
    (setq ltype-file "MT_MP_lines")
    (if (null (tblsearch "Ltype" MY-LT))
        (command "_-linetype" "_load" MY-LT ltype-file "")
    )
	
    (setq layerName "A_Slopes")
    (if (tblsearch "layer" layerName)
      	(command "_layer" "_Set" layerName "")
      	(command "_layer" "_set" "0" "_Make" layerName "_LWeight" "0.25" layerName "_Color" "91" layerName "_set" layerName "")
    )
	
    (setvar "celtype" MY-LT)
    (setvar "cecolor" "256")
    (setvar "CELWEIGHT" -1)

    (command "_.pline")
    (while (= 1 (getvar "CMDACTIVE"))
         (command pause)
    ); end while
    (setvar "celtype" "ByLayer")
    (setvar "cmdecho" oldEcho)
    (princ)
)

(defun c:gp-est (/ MY-LT ltype-file layerName)
;��������� ��������

    (setq oldEcho (getvar "cmdecho"))
    (setvar "cmdecho" 0)
	
    (setq MY-LT "���-��������")
    (setq ltype-file "MT_MP_lines")
    (if (null (tblsearch "Ltype" MY-LT))
        (command "_-linetype" "_load" MY-LT ltype-file "")
    )
	
    (setq layerName "A_PipeRacks")
    (if (tblsearch "layer" layerName)
      	(command "_layer" "_Set" layerName "")
      	(command "_layer" "_set" "0" "_Make" layerName "_LWeight" "0.25" layerName "_Color" "7" layerName "_set" layerName "")
    )
	
    (setvar "celtype" MY-LT)
    (setvar "cecolor" "256")
    (setvar "CELWEIGHT" -1)

    (command "_.pline")
    (while (= 1 (getvar "CMDACTIVE"))
         (command pause)
    ); end while
    (setvar "cmdecho" oldEcho)
    (princ)
)

(defun c:gp-vor (/ P1 P2 N1 ans b_name fname layerName oldEcho)
;������� �����

	(setq oldEcho (getvar "cmdecho"))
	(setvar "cmdecho" 0)
	(setvar "attdia" 1)
	(setvar "mirrtext" 0)

	(setq layerName "A_Fences")
	(if (tblsearch "layer" layerName)
		(command "_layer" "_Set" layerName "")
		(command "_layer" "_set" "0" "_Make" layerName "_LWeight" "0.7" layerName "_Color" "252" layerName "_set" layerName "")
	)
	
	(setvar "celtype" "ByLayer")
	(setvar "cecolor" "256")
	(setvar "CELWEIGHT" -1)
	
	(setq b_name "VOR_45")
	(setq fname "lib_MP1.dwg")
	(if (not (tblsearch "block" b_name))
		(MT-importBlockFromLib b_name fname)
	)
	(setq P1 (getpoint "\n������� ����� ������� <�����> : "))
	(princ)
	(if (= P1 nil)
		(setq N1 1) (setq N1 0)
	)
;
	(while (= N1 0)
		(command "_.insert" b_name P1 1 1)
		(while (= 1 (getvar "CMDACTIVE"))
			(command pause)
		); end while
		(setq P1 (getpoint "\n������� ����� ������� <�����> : "))
		(princ)
		(if (= P1 nil)
			(setq N1 (1+ N1)) (setq N1 0)
		)
		(princ)
	)
	(setvar "cmdecho" oldEcho)
)

(defun c:gp-kl (/ P1 P2 N1 ans b_name fname oldEcho layerName)
;������� �������

	(setq oldEcho (getvar "cmdecho"))
	(setvar "cmdecho" 0)
	(setvar "attdia" 1)
	(setvar "mirrtext" 0)
	
	(setq layerName "A_Fences")
	(if (tblsearch "layer" layerName)
		(command "_layer" "_Set" layerName "")
		(command "_layer" "_set" "0" "_Make" layerName "_LWeight" "0.7" layerName "_Color" "252" layerName "_set" layerName "")
	)
	
	(setvar "celtype" "ByLayer")
	(setvar "cecolor" "256")
	(setvar "CELWEIGHT" -1)

	(setq b_name "KL_09")
	(setq fname "lib_MP1.dwg")
	(if (not (tblsearch "block" b_name))
		(MT-importBlockFromLib b_name fname)
	)
	
	(setq P1 (getpoint "\n������� ����� ������� <�����> : "))
	(princ)
	(if (= P1 nil)
		(setq N1 1) (setq N1 0)
	)
;
	(while (= N1 0)
		(command "_.insert" b_name P1 1 1)
		(while (= 1 (getvar "CMDACTIVE"))
			(command pause)
		); end while
		(setq P1 (getpoint "\n������� ����� ������� <�����> : "))
		(princ)
		(if (= P1 nil)
			(setq N1 (1+ N1)) (setq N1 0)
		)
		(princ)
	)
	(setvar "cmdecho" oldEcho)
)

(defun c:gp-est-p (/ P1 N1 *A1 A2 userclick ans b_name fname oldEcho dcl_id)
;��������- ��������

  (setq oldEcho (getvar "cmdecho"))
  (setvar "cmdecho" 0)
  (setvar "attdia" 1)
  (setvar "mirrtext" 0)
	
;��������� �������� ����������
 (setq *A1 (list "500" "1000" "2000" "1"))
;�������� �������
  (setq dcl_id (load_dialog "MT_main.dcl"))
  (new_dialog "common_mark" dcl_id)

  (start_list "A1")
  (mapcar 'add_list *A1)
  (end_list)
;;
  (action_tile "A1" "(setq *A2 (get_tile \"A1\"))")
  (action_tile "accept" "(setq *A2 (get_tile \"A1\")) (SETQ userclick 1) (done_dialog)")
  (action_tile "cancel" "(SETQ userclick 0) (done_dialog)")
;;
  (start_dialog)
  (unload_dialog dcl_id)

  (if (= *A2 "") ;�� ������
    (setq A2 1)
    (setq A2 (/ (atof (nth (atoi *A2) *A1)) 1000))
  )
  (if (= A2 0.001)
		(setq A2 1)
  )
; 
;�������� ���������
 (IF (= userclick 1)
	(PROGN
		(setq layerName "A_PipeRacks")
		(if (tblsearch "layer" layerName)
			(command "_layer" "_Set" layerName "")
			(command "_layer" "_set" "0" "_Make" layerName "_LWeight" "0.25" layerName "_Color" "7" layerName "_set" layerName "")
		)
		
		(setvar "celtype" "ByLayer")
		(setvar "cecolor" "256")
		(setvar "CELWEIGHT" -1)
		
		(setq fname "lib_MP1.dwg")
		(setq b_name "EST_P")
		(setq fname "lib_MP1.dwg")
		(if (not (tblsearch "block" b_name))
			(MT-importBlockFromLib b_name fname)
		)
		
		(command "_osmode" 1015)
		
		(setq P1 (getpoint "\n������� ����� ������� <�����> : "))
		(princ)
		(if (= P1 nil)
			(setq N1 1) (setq N1 0)
		)
	;
	   (while (= N1 0)
			(command "_ortho" "on")
			(command "_osmode" 0)
			(command "_.insert" b_name P1 A2 A2)
			(while (= 1 (getvar "CMDACTIVE"))
				(command pause)
			); end while
			(command "_osmode" 1015)
			(setq P1 (getpoint "\n������� ����� ������� <�����> : "))
			(princ)
			(if (= P1 nil)
				(setq N1 (1+ N1)) (setq N1 0)
			)
			(princ)
		)
		(command "_osmode" 1015)
	)
  )
  (setvar "cmdecho" oldEcho)
)

(defun c:gp-ss (/ blockName fileName keyName layerName oldEcho oldOsmode)
;������� ������������ �����

	(setq oldEcho (getvar "cmdecho"))
	(setvar "cmdecho" 0)
	(setq oldOsmode (getvar "osmode"))
	
	(vl-load-com)
	
	(setq layerName "A_Grid")
	(if (tblsearch "layer" layerName)
		(command "_layer" "_Set" layerName "")
		(command "_layer" "_set" "0" "_Make" layerName "_LWeight" "0.25" layerName "_Color" "161" layerName "_set" layerName "")
	)

	(setvar "celtype" "ByLayer")
	(setvar "cecolor" "256")
	(setvar "CELWEIGHT" -1)
	
	(setq blockName "GRID")
	(setq fileName "lib_MP1.dwg")
	
	(if (not (tblsearch "block" blockName))
		(MT-importBlockFromLib blockName fileName)
	)
	(vl-cmdf "_.-insert" blockName pause 1.0 1.0 0)
	(vl-cmdf "_explode" (entlast))

	(setvar "osmode" oldOsmode)
	(setvar "cmdecho" oldEcho)
	(princ "\n���������")
	(princ)
)

(defun c:gp-stair (/ blockName fileName keyName layerName oldEcho oldOsmode scl P1 qwe N)
;������� �������

	(setq oldEcho (getvar "cmdecho"))
	(setvar "cmdecho" 0)
	(setq oldOsmode (getvar "osmode"))
	
	(vl-load-com)
	
	(setq layerName "A_Stairs")
	(if (tblsearch "layer" layerName)
		(command "_layer" "_Set" layerName "")
		(command "_layer" "_set" "0" "_Make" layerName "_LWeight" "0.25" layerName "_Color" "141" layerName "_set" layerName "")
	)

	(setvar "celtype" "ByLayer")
	(setvar "cecolor" "256")
	(setvar "CELWEIGHT" -1)
	(setvar "osmode" 55);
	
	(setq blockName "STAIR")
	(setq fileName "lib_MP1.dwg")
	
	(if (not (tblsearch "block" blockName))
		(MT-importBlockFromLib blockName fileName)
	)
	
	(setq P1 (getpoint "\n������� ����� ������� <�����> : "))
	(princ)
	(if (= P1 nil)
		(setq N 1) (setq N 0)
	)
	(while (= N 0)
	
		(setq qwe (getreal "\n\n������ (�) <1.0>: "))
		(if (= qwe nil)
				   (setq scl 1.0)
				   (setq scl qwe)
		)
		
		(vl-cmdf "_.-insert" blockName P1 1.0 scl pause)
		
		(setq P1 (getpoint "\n������� ����� ������� <�����> : "))
		(princ)
		(if (= P1 nil)
			(setq N 1) (setq N 0)
		)
		
	)

	(setvar "osmode" oldOsmode)
	(setvar "cmdecho" oldEcho)
	(princ "\n���������")
	(princ)
)

(load (findfile "MT_main.lsp"))
