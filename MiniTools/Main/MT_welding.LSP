;; Copyright (c) Maks Tulin 2008-2012/2018
;;
;; ��������� ��������� ������� ����
;;


(defun MT-welding (/ MY-LT ltype-file keyName userclick ans N1 N2 P1 A1 B1 C1)

    (setvar "cmdecho" 0)
    (setq A1 1)
    (setq B1 1)
    (setq C1 1)
    (setq N1 0)
    (while (= N1 0)
;�������� �������
      (setq dcl_id (load_dialog "MT_main.dcl"))
      (new_dialog "Welding" dcl_id)

; ���������� �������� ���������� �� ���������
      (if (= A1 1)
          (set_tile "AA1" "1")
          (set_tile "AA2" "1")
      )
      (if (= B1 1)
          (set_tile "BB1" "1")
          (set_tile "BB2" "1")
      )
      (if (= C1 1)
          (set_tile "CC1" "1")
          (set_tile "CC2" "1")
      )
;���������� ���������� ���������������� ��������
      (defun sper (/)
          (if (= "1" (get_tile "AA1")) 
              (setq A1 1) (setq A1 0)
          )
          (if (= "1" (get_tile "BB1")) 
              (setq B1 1) (setq B1 0)
          )
          (if (= "1" (get_tile "CC1")) 
              (setq C1 1) (setq C1 0)
          )
      )
;;
     (action_tile "accept" "(SETQ userclick 1) (sper) (done_dialog)")
     (action_tile "cancel" "(SETQ userclick 0) (done_dialog)")
;;
     (start_dialog)
     (unload_dialog dcl_id)
     (princ)
; 
;�������� ���������

     (IF (= userclick 1)
       (PROGN
         (setq N2 0)
	 (while (= N2 0)
	    (progn
              (if (= P1 nil)
                    (setq P1 (getpoint "\n \n������� ������ ��� (�������) "))
	      )
	      (princ)
              (if (= P1 nil)
                   (setq N2 (1+ N2)) (setq N2 0)
              )
;
              (if (and (= A1 1) (= B1 1) (= C1 1))
                    (setq MY-LT "SVR1")
              )
              (if (and (= A1 1) (= B1 1) (= C1 0))
                    (setq MY-LT "SVR2")
              )
              (if (and (= A1 1) (= B1 0) (= C1 1))
                    (setq MY-LT "SVR3")
              )
              (if (and (= A1 1) (= B1 0) (= C1 0))
                    (setq MY-LT "SVR4")
              )
              (if (and (= A1 0) (= B1 1) (= C1 1))
                    (setq MY-LT "SVR5")
              )
              (if (and (= A1 0) (= B1 1) (= C1 0))
                    (setq MY-LT "SVR6")
              )
              (if (and (= A1 0) (= B1 0) (= C1 1))
                    (setq MY-LT "SVR7")
              )
              (if (and (= A1 0) (= B1 0) (= C1 0))
                    (setq MY-LT "SVR8")
              )
;
              (setq ltype-file "MT_lines")
              (if (null (tblsearch "Ltype" MY-LT))
                     (command "_-linetype" "_load" MY-LT ltype-file "")
              )
			  
			  (setq keyName "_weldLay")
			  (MT-createLayer keyName)
			  
              (setvar "celtype" MY-LT)
              (setvar "cecolor" "256")
              (setvar "CELWEIGHT" -1)
              (setvar "CELTSCALE" 1)
;              (command "_osmode" 1015)
;
              (command "_.pline" P1)
              (while (= 1 (getvar "CMDACTIVE"))
                   (command pause)
              ); end while
              (command "_regen")
;
              (setq P1 (getpoint "\n \n������� ������ ��� (�������) "))
	      (princ)
              (if (= P1 nil)
                   (setq N2 (1+ N2)) (setq N2 0)
              )
	   );progn
	 );while
       );progn
       (setq N1 (1+ N1))
     );if
  )
  (setvar "celtype" "ByLayer")
  (setvar "cmdecho" 1)
  
  (princ "\n���������")
  (princ)
)
