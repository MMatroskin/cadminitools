; ����� �� �����
; "AutoLISP � Visual LISP � ����� AutoCAD"
; �.�.�������, �.�.��������.
;
; http://www.private.peterlink.ru/poleshchuk/cad/index.html
;
; ����������� ������ ���������
;

(defun PL:String-Rep (str old new / pos)

  (if (setq pos (vl-string-search old str))
		(strcat 
			(substr str 1 pos) 
			new
			(PL:String-Rep 
				(substr
					str 
					(+ (strlen old) pos 1)
				)
				old 
				new
			)
		)
	str)
)
