; ������� ��������� � ���������� IAxDbDocument (ObjectDBX)
; ��������: http://autolisp.ru/2010/04/08/proceed-unactive-document/
; ���������� ������ � ����������� ObjectDBX ��� ACAD 2002-2004
; ������� ������������ ��� ����� �������
; �����: (setq odbx (_lispru-odbx))


(defun _lispru-odbx (/)
                    ;|
*    ������� ���������� ��������� IAxDbDocument (��� ������ � ������� DWG ���
* �� ��������). ���� ��������� �� ��������������, ���������� nil. ���������
* �� ACAD 2002, 2004, 2005, 2006, 2007, 2008, 2010
*    ����� - Fatty aka ���� jr. ����� ������ ��������� ��� ����� ������� �
* ��������������
*    ��������� ������:
* ���
*    ������� ������:
(_lispru-odbx)
|;

  (cond
		((< (atof (getvar "acadver")) 15.06)
			(alert
				"ObjectDBX method not applicable\nin this AutoCAD version"
			) ;_ end of KPBLC-MSG-ALERT
			nil
		)
		((= (fix (atof (getvar "acadver"))) 15)
			(if (not (vl-registry-read "HKEY_CLASSES_ROOT\\ObjectDBX.AxDbDocument\\CLSID")) ;_ end of not
				(startapp "regsvr32.exe" (strcat "/s \"" (findfile "axdb15.dll") "\"")) ;_ end of startapp
			) ;_ end of if
			(vla-getinterfaceobject
				(vlax-get-acad-object)
				"ObjectDBX.AxDbDocument"
			) ;_ end of vla-getinterfaceobject
		)
		(t
			(vla-getinterfaceobject
				(vlax-get-acad-object)
				(strcat "ObjectDBX.AxDbDocument." (itoa (fix (atof (getvar "acadver")))))
			) ;_ end of vla-getinterfaceobject
		)
    ) ;_ end of cond
) ;_ end of defun
