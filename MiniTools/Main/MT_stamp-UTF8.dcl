stamp_0 : dialog{
	width=72;
	fixed_width = true;
	key = "Title";
	label = "Заполнение основной надписи";
	:row {
		:column{
			:boxed_column{
				:edit_box {
					label = "Шифр";
					key = "Title_0";
					edit_width=64;
					height = 1.1;
					fixed_height = true;
				}
				:edit_box {
					label = "Объект";
					key = "Subject_0";
					edit_limit = 256;
					edit_width=64;
					height = 1.1;
					fixed_height = true;
				}
				:edit_box {
					label = "Корпус";
					key = "Comments_0";
					edit_limit = 256;
					edit_width=64;
					height = 1.1;
					fixed_height = true;
				}
				:edit_box {
					label = "Стадия";
					key = "Keywords_0";
					edit_width=64;
					height = 1.1;
					fixed_height = true;
				}
				:edit_box {
					label = "Дата выпуска";
					key = "_release_date_0";
					edit_width=64;
					height = 1.1;
					fixed_height = true;
				}
				spacer;
			}
			:boxed_column{
				:edit_box {
					label = "Разработал";
					key = "_author1_0";
					edit_width=64;
					height = 1.1;
					fixed_height = true;
				}
				:edit_box {
					label = "Разработал";
					key = "_author2_0";
					edit_width=64;
					height = 1.1;
					fixed_height = true;
				}
				:edit_box {
					label = "Проверил";
					key = "_inspector_0";
					edit_width=64;
					height = 1.1;
					fixed_height = true;
				}
				:edit_box {
					label = "Начальник отдела";
					key = "_head_of_dept_0";
					edit_width=64;
					height = 1.1;
					fixed_height = true;
				}
				:edit_box {
					label = "Hорм. контроль";
					key = "_standard_control_0";
					edit_width=64;
					height = 1.1;
					fixed_height = true;
				}
				:edit_box {
					label = "ГИП";
					key = "_project_manager_0";
					edit_width=64;
					height = 1.1;
					fixed_height = true;
				}
				spacer;
			}
			:boxed_column{
				:edit_box {
					label = "Организация";
					key = "_company_0";
					edit_width=64;
					height = 1.1;
					fixed_height = true;
				}
				spacer;
			}
		}
	}
	:row{
		:toggle{
			key = "togSepStr";
			value = "0";
			fixed_height = true;
			alignment = left;
			label = "Сохранять разбиение на строки ( символ ' \\n' )";
		} 
	}
	:row{
		:spacer{
			width=48;
		}
		ok_cancel;
	}
}
