;; Copyrighted by Maks Tulin 2018
;;

(defun MT-setupLayers (/ layersConfig sectionName sectionNamesList oldEcho dcl_id A1 st
					userclick layersConfigList layerPropertyList layersList)
; ��������������� ��� �������� ������ �����

	(setq oldEcho (getvar "cmdecho"))
	(setvar "cmdecho" 0)
	
	(vl-load-com)
	(setq layersConfig "MT_layers.cfg")

	(if (findfile layersConfig)
		(progn
			(setq sectionNamesList (MT-getSectList layersConfig))
			(setq sectionNamesList (vl-remove "common_default" sectionNamesList))
			;(setq sectionNamesList (vl-remove "common" sectionNamesList))
			
			(if (/= sectionNamesList nil)
				(progn
					(setq dcl_id (load_dialog "MT_main.dcl"))
					(new_dialog "selection_list" dcl_id)

					(start_list "A1")
					(mapcar 'add_list sectionNamesList)
					(end_list)

					(action_tile "A1" "(setq st (get_tile \"A1\"))")
					(action_tile "accept" "(setq st (get_tile \"A1\")) (SETQ userclick 1) (done_dialog)")
					(action_tile "cancel" "(SETQ userclick 0) (done_dialog)")

					(start_dialog)
					(unload_dialog dcl_id)
					
					(if (and 
							(= st "")
							(= userclick 1)
						)
						(progn
							(princ "\n������ ������! ")
							(SETQ userclick 0)
						)

					)
					
					(if (= userclick 1)
						(progn
						
							(setq sectionName (nth (atoi st) sectionNamesList))
							(princ sectionName)
							
							(setq layersConfigList (MT-getConfigList layersConfig sectionName))
							
							(foreach x layersConfigList
								(setq layerPropertyList
									(MT-stringToList 
										(cdr x)
										","
									)
								)
								(setq layersList (append layersList (list layerPropertyList)))
							)
							
							(foreach x layersList
								(vl-cmdf "_layer"
										"_set" "0"
										"_Make" (car x) 
										"_LWeight" (atof (car (cdr x))) (car x) 
										"_Color" (atoi (cadr (cdr x))) (car x) 
										"_Ltype" (caddr (cdr x)) (car x) 
										"_Plot" (cadddr (cdr x)) (car x) 
										""
								)
							) ; create and redefine layers
							
							(setvar "clayer" "0")
						)
					)
				)
			)
		)
		(princ "\n������ ��������! ���� �� ������! ")
	)
	
	
	(setvar "cmdecho" oldEcho)
	(princ "\n���������")
	(princ)
)

(load (findfile "MT_main.lsp"))
