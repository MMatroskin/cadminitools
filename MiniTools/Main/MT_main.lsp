;; Copyright (c) Maks Tulin 2011/2018
;;
;; ���������� �������� �������
;;

(defun *error* (msg)
;; ����������������� ��������� ���������� � ��������� ���������
;; ��������� �� ���������.

		; (if (and lastEnt (not (equal lastEnt (entlast))))
			; (command "_.erase" (entlast) "")
		; )
		
		(if oldMlStyle
			(progn
				(setvar "CMLEADERSTYLE" oldMlStyle)
				(setq oldMlStyle nil)
			)
		)
		
		(setvar "celtype" "ByLayer")
		(setvar "cecolor" "256")
		(setvar "CELWEIGHT" -1)
		
		(setvar "attdia" 1)
		(setvar "filedia" 1)
		(setvar "mirrtext" 0)
		(setvar "attreq" 1)
		(setvar "MENUECHO" 0)
		(setvar "NOMUTT" 0)
		
		(MT-restoreSysvars)
		
		(setvar "CMDECHO" 1)
		
		(terpri)
		(princ msg)
		
); end of *error*

(defun MT-extlib-insert( / configFileName sectionName path fileName acadDoc odbx lst dcl_id st str oldEcho cnt)
; ����� ����� ���������� � ����������� �� ���� ����� �� ��������
; ������� �� ������� ����

	(setq oldEcho (getvar "cmdecho"))
	(setvar "cmdecho" 0)
	
	(setq configFileName 
		(findfile 
			"MiniTools.cfg"
		)
	)
	(setq sectionName "main")
	(setq path 
		(MT-getValue configFileName sectionName "_libsPath")
	)
	(if (or (= path "") (= path nil))
		(setq path
			(strcat
				(MT-getValue configFileName sectionName "_path")
				"\\MiniTools\\Plugins\\lib"
			)
		)
	)
	(setq path (strcat path "\\"))
	(setq fileName
		(getfiled "Select a DWG File" path "dwg" 8)
	)
	(if (/= fileName nil)
		(progn
			(setq cnt T)
			(vl-load-com)
			(setq acadDoc (vla-get-activedocument (vlax-get-acad-object)))
			(setq lst (MT-getCommentsBlockInLib fileName))
			
			(while cnt
			
				(setq dcl_id (load_dialog "MT_main.dcl"))
				(new_dialog "selection_list" dcl_id)

				(start_list "A1")
				(mapcar 'add_list lst)
				(end_list)

				(action_tile "A1" "(setq st (get_tile \"A1\"))")
				(action_tile "accept" "(setq st (get_tile \"A1\")) (SETQ userclick 1) (done_dialog)")
				(action_tile "cancel" "(SETQ userclick 0) (done_dialog)")

				(start_dialog)
				(unload_dialog dcl_id)
				
				(if (and 
						(= st "")
						(= userclick 1)
					)
					(progn
						(princ "\n������ ������! ")
						(SETQ userclick 0)
					)

				)

				(if (= userclick 1)
					(progn
						(setq str (nth (atoi st) lst))
						(princ str)
						
						(setq blockName (MT-getBlockNameIfComment fileName str))					
						
						(if (not (tblsearch "block" blockName))
							(MT-importBlockFromLib blockName fileName)
						)
						(MT-insertBlock blockName)
					)
					(setq cnt nil)
				)
			)
		)
	)
		
	(setvar "cmdecho" oldEcho)
	(princ)
)

(defun MT-getBlockNameIfComment (fileName str / odbx acadDoc x res)
;; ����� ����� �� ����������

	(vl-load-com)
	(setq acadDoc (vla-get-activedocument (vlax-get-acad-object)))
	(if (setq fileName (findfile fileName))
		(progn
			(vla-startundomark acadDoc)
			(load (findfile "_lispru-odbx.lsp"))
			(setq odbx (_lispru-odbx))
			(vla-open odbx fileName)
			(not
				(vl-catch-all-error-p
					(vl-catch-all-apply
						(function
							(lambda ()
								(vlax-for x (vla-get-blocks odbx)
									(if (= (vla-get-Comments x) str)
										(setq res (vla-get-name x))
									); ��� ����� (���� � ���������� ������ ���������� ������ - ���������
								)
							)
						)
					)
				)
			)
			(vlax-release-object odbx)
			(vla-endundomark acadDoc)
		)
	)
	(gc)
	res
)

(defun MT-getCommentsBlockInLib (fileName / odbx acadDoc x item res)
;; ������ ����������� � ������ ��� ���������� �����

	(vl-load-com)
	(setq acadDoc (vla-get-activedocument (vlax-get-acad-object)))
	(if (setq fileName (findfile fileName))
		(progn
			(vla-startundomark acadDoc)
			(load (findfile "_lispru-odbx.lsp"))
			(setq odbx (_lispru-odbx))
			(vla-open odbx fileName)
			(not
				(vl-catch-all-error-p
					(vl-catch-all-apply
						(function
							(lambda ()
								(vlax-for x (vla-get-blocks odbx)
									(if (and 
											(= (vla-get-islayout x) :vlax-false)
											(/= (setq item (vla-get-Comments x)) "")
										)
										(setq res (cons item res))
									); ������ ������������ � ������ � ����������
								)
								(reverse res)
							)
						)
					)
				)
			)
			(vlax-release-object odbx)
			(vla-endundomark acadDoc)
		)
	)
	(gc)
	res
)

(defun MT-importBlockFromLib (blkName fileName / odbx acadDoc)
;; ������ � ������� ������ ����� �� �������� �����
;; ��� ������� � ������� ���� �� ����������������

	(vl-load-com)
	(setq acadDoc (vla-get-activedocument (vlax-get-acad-object)))
	(if (setq fileName (findfile fileName))
		(progn
			(vla-startundomark acadDoc)
			(load (findfile "_lispru-odbx.lsp"))
			(setq odbx (_lispru-odbx))
			(vla-open odbx fileName)
			(not
				(vl-catch-all-error-p
					(vl-catch-all-apply
						(function
							(lambda ()
								(vla-copyobjects odbx
									(vlax-safearray-fill
										(vlax-make-safearray vlax-vbobject '(0 . 0))
										(list (vla-item	(vla-get-blocks odbx) blkName))
									)
									(vla-get-blocks acadDoc)
								)
							)
						)
					)
				)
			)
			(vlax-release-object odbx)
			(setq odbx nil)
			(vla-endundomark acadDoc)
		)
	)
	(gc)
	(terpri)
)

(defun MT-exportBlockFromLib (blockName fileNameIn fileNameOut / odbx acadDoc tmpFile fileName ent obj space x)
;; ������� ����� �� ���������� � ����

	(vl-load-com)
	(setq acadDoc (vla-get-activedocument (vlax-get-acad-object)))
	(if (setq fileName (findfile fileNameIn))
		(progn
			(vla-startundomark acadDoc)
			
			(setq tmpFile (vla-add(vla-get-documents(vlax-get-acad-object))))
			(load (findfile "_lispru-odbx.lsp"))
			(setq odbx (_lispru-odbx))
			(vla-open odbx fileName)
			(not
				(vl-catch-all-error-p
					(vl-catch-all-apply
						(function
							(lambda ()
								(vla-copyobjects odbx
									(vlax-safearray-fill
										(vlax-make-safearray vlax-vbobject '(0 . 0))
										(list (vla-item	(vla-get-blocks odbx) blockName))
									)
									(vla-get-blocks tmpFile)
								)
								(setq space (vla-get-modelspace tmpFile))
								(setq ent (vlax-invoke space 'insertblock '(0.0 0.0 0.0) blockName 1.0 1.0 1.0 0))
								(vlax-for ent space 
									(if (and
											(vlax-method-applicable-p ent 'explode)
											(vlax-property-available-p ent 'name)
											(= (vla-get-name ent) blockName)
										)
										(progn 
											(vla-explode ent)
											(vla-delete ent)
										)
									)
								)
								(foreach x '("Blocks" "DimStyles" "Linetypes"	"Layers" "TextStyles")
									(vlax-for obj (vlax-get tmpFile x)
										(if (and 
												(not (wcmatch (vla-get-name obj) "`*D*"))
												(or 
													(not (vlax-property-available-p obj 'IsLayout))
													(= (vla-get-IsLayout obj) :vlax-false)
												)
											)
											(vl-catch-all-apply 'vla-Delete (list obj))
										)
									)
								)
							)
						)
					)
				)
			)
			(vla-saveas tmpFile fileNameOut)
			(vla-close tmpFile)
			(vlax-release-object odbx)
			(setq odbx nil)
			(vla-endundomark acadDoc)
		)
	)
	(gc)
	(terpri)
)

(defun MT-insertBlock (b_nameIn / oldEcho osmodeBak b_name b_path b_pnt ANG scl item subItem tmp)
; ������� ����� (� �.�.� ����������)
; ��� ������� �� ����� ��������� ������������ ����� - ����������� ����, ������������� ���� � �������
; b_nameIn- ��� ����� ��� ������- ���/����� �������/����
; ����� : (MT-insertBlock "Logo")/(MT-insertBlock ("Logo" '(56.4379 22.9072) angle scale))

	(setq oldEcho (getvar "CMDECHO"))
	(setvar "CMDECHO" 0)
	(setq osmodeBak (getvar "osmode"))
	
	(setvar "attdia" 1)
	(setvar "osmode" 55)
	(setvar "attreq" 0)
;	(setvar "NOMUTT" 1)

	(vl-load-com)
	
	(if (listp b_nameIn)
		(progn
			(if (null (setq scl (cadddr b_nameIn)))
				(setq scl 1.0)
			)
			(if (null (setq ANG (caddr b_nameIn)))
				(setq ANG 0)
			)
			(if (null (setq b_pnt (cadr b_nameIn)))
				(setq b_pnt nil)
			)
			(setq b_name (car b_nameIn))
		)
		(progn
			(setq b_name b_nameIn)
			(setq ANG 0)
			(setq scl 1.0)
			(setq b_pnt nil)
		)
	)
	
	(if (/= (setq b_path (findfile (strcat b_name ".dwg"))) nil)
		(setq b_name b_path)
	)
	
	; ��������� ������ ��� ���������� ������� 
	; � ��������� ������������  � ������� ��� �����
	(setq tmp (ssadd))
	(vl-cmdf "_point"  "0,0,0")
	(ssadd (entlast) tmp)
	
	(if (null b_pnt)
		(vl-cmdf "_.-insert" b_name pause scl scl ANG)
		(vl-cmdf "_.-insert" b_name b_pnt scl scl ANG)
	); �� ������� scale uniform � ��������� ����� !!
  
	(setq item (entlast))
	(if (eq (cdr (assoc 2 (entget item))) b_name)
		(if (setq subItem (entnext item))
			(if (eq (cdr (assoc 0 (entget subItem))) "ATTRIB")
				(vl-cmdf "_eattedit" item)
			)
		)
		
	)
	
	(entdel (ssname tmp 0))	; ������� ��������� ������
	
	(setvar "attreq" 1)
;	(setvar "NOMUTT" 0)
	(setvar "osmode" osmodeBak)
	(setvar "CMDECHO" oldEcho)
	(princ " \ndone ")

)

(defun MT-updateBlock ( / blk b_list b_path )
; ���������� ���� ������ � ������� (� �.�.� ���������� ) �� ��������
; ������� - ����������� ����� DWG, ��������� �� ����� ���������

	(vl-load-com)
	(if (setq blk (tblnext "BLOCK" T ))
		(progn
			(setq b_list (list (cdr (assoc 2 blk ))))
			(while (setq blk (tblnext "BLOCK" nil ))
				(if (= (vl-string-search "|" (cdr (assoc 2 blk ))) nil); �������� �� xref
					(setq b_list (append b_list (list (cdr (assoc 2 blk )))))
				)
			)
			(foreach b_name b_list
				(if (setq b_path (findfile (strcat b_name ".dwg")))
					(progn 
						(vl-cmdf "_.-insert" (strcat b_name "=" b_path) "_y")
						(vl-cmdf)
						(vl-cmdf "_AttSync" "_name" b_name)
					)
					(princ (strcat "\n" b_name ".dwg Not found" )); ������� ����� � ������??
				)
			)
		)
		(princ "\n Block definition is not found in a drawing")
	)
	
	(princ)
)


(defun MT-insertOrientBlock( blockName / osmodeOld P0 P1 N1 mir normalLine item)
; ������� ����� ���������� �� ����������
; ����� (MT-insertOrientBlock blockName)

	(setq osmodeOld (getvar "osmode"))
	(setvar "osmode" 0)

	(vl-load-com)
	(setq P0 (getpoint "\n������� ����� ������� <B����>: "))
	(princ)
	(if (= P0 nil)
		(setq N1 1) (setq N1 0)
	)
	
	(while (= N1 0)
		(setq P1 (getpoint P0 "\n������� �����������"))
		(if (or (= P0 nil) (= P0 P1))
			(progn
				(*error* "\n������������ ����!")
				(exit)
			)
		)
		
		(setq ang (angle P0 P1))
		(setq ang
			(/
			  	(* ang 180)
				pi
			)
		)
	  
		(if
			(or
				(> (car P0) (car P1))
				(and 
					(= (car P0) (car P1))
					(> (cadr P0) (cadr P1))
				)
			)
				(progn
					(setq mir T)
					(setq ang (+ ang 180))
				)
				(setq mir nil)
		)
		
		(MT-insertBlock (list blockName P0 ang 1))
		(if mir
			(progn
				(setq item (entlast))
				(setvar "mirrtext" 0)
				(setq normalLine (MT-getNormal2Line P0 P1))
				(vl-cmdf "_mirror" item "" (car normalLine) (cdr normalLine) "_y")
			)
		)
		
		(setq P0 (getpoint "\n������� ����� �������"))
		(princ)
		(if (= P0 nil)
			(setq N1 (1+ N1)) (setq N1 0)
		)
		(princ)
	)
	
	(setvar "osmode" osmodeOld)
)

(defun MT-editAttributeInBlock (item attName attValue / tmpItem entListCur entListNew)
; �������������� ��������� � �����
; ���� ����������� ���� - ����� ������ ��������� regenall
; ����� (MT-editAttributeInBlock item attName attValue)
; item - ename ������� 
; attName - ��� ���������
; attValue - ����� �������� ���������
; ������ (MT-editAttributeInBlock item "FIELD" str)

	(setq tmpItem (entnext item))
    (while (eq 
			(cdr (assoc 0 (entget tmpItem))) 
			"ATTRIB"); ���� �������� ��������� ���������� � �������� �� ���� ("ATTRIB")
        (if (= (cdr (assoc 2 (entget tmpItem))) attName)
			(progn
				(setq entListCur (entget tmpItem))
				(setq entListNew (subst (cons 1 attValue) (assoc '1 entListCur) entListCur))
				(entmod entListNew)
			)
		) ; �������� �������� ������� ��������
		(setq tmpItem (entnext tmpItem)) ; next attrib
    )
)

(defun MT-getNewFieldValue (item attName / entId ent entList result)
; ��������� ������ ���� �� �������� ��������� ����� (���. �� �������)
; call (MT-getNewFieldValue "TITLE")

	(load "_GetObjId.LSP")

	(setq ent (entnext item))
	(setq entList (entget ent))
	(while (and ent (= (cdr (assoc 0 entList)) "ATTRIB"))
		(if (= (cdr (assoc 2 entList)) attName)
			(setq entId (GET-OBJECTID-X86-X64 ent))
		)
		(setq ent (entnext ent))
		(setq entList (entget ent))
	); ID ������������ ���������
	(setq result (strcat "%<\\AcObjProp Object(%<\\_ObjId " entId ">%).TextString>%"))
)

(defun MT-startApplication (App_Name par_name /)
; ������ �������� ����������
; ��������� - ���� � ������������ ����� � ������ ���������� ���.������

	(setq App_Name (changeSlash App_Name))
	(setq App_Name (findfile App_Name))
	(if (/= App_Name nil)
		(startapp App_Name par_name)
		(princ "\n���� �� ������")
	)
	(gc)
)

(defun MT-getDwgFullName (/ result)
; ��������� ������� ����� �������� �������

	(setq result 
		(strcat 
			(getvar "dwgprefix") 
			(getvar "dwgname")
		)
	)
)

(defun MT-changeSlash (StringIn / result temp i1)
; ������ ����� �� ���� ���-�������

	(setq result "")
	(setq i1 1)
	(while (<= i1 (strlen StringIn))
		(setq temp (substr StringIn i1 1))
		(if (= temp "/")
			(setq result (strcat result "\\"))
			(setq result (strcat result temp))
		);if
		(setq i1 (1+ i1))
	);while
	result
)

(defun MT-changeSubstr (str src dst / result pos i0)
; ������ ���������

	(vl-load-com)
	(setq result str)
	(setq i0 0)
	(while (/= (setq pos (vl-string-search src result i0)) nil)
		(setq result (vl-string-subst dst src result i0))
		(setq i0 (+ pos (strlen dst)))
	);while
	result
)

(defun MT-getPairFromString (str delim / pos key value result)
; ��������������� ���� �� ������ � ������������

	(vl-load-com)
	(setq pos(vl-string-search "=" str))
	(setq key (substr str 1 pos))
;  	(princ key)
	(setq value (substr str (+ pos 2)))
;  	(princ value)
	(setq result (cons key value))
)

(defun MT-stringToList (str sep / tmpStr pos result)
;; �������������� ������ � ������ �� �����������
;; �����: (MT-stringToList "��� \r\n������������� \r\n�����" "\r\n")
	
	(while (setq pos (vl-string-search sep str))
		(setq tmpStr (substr str 1 pos))
;;;		(if (or (= (strlen tmpStr) 0) (= tmpStr " "))
;;;			(setq tmpStr "_emptyString_")
;;;		)
		(setq result (cons tmpStr result))
		(setq str (substr str (+ (strlen sep) 1 pos)))
	)
	(setq result (cons str result))
	
	(reverse result)
)

(defun MT-getAcadVer ( / aVer str)
	
	(setq str (MT-getNumberAcadVer))
	(setq aVer (atof str))
	
	(cond
		((= aVer 23.0) "2019")
		((= aVer 22.0) "2018")
		((= aVer 21.0) "2017")
		((= aVer 20.1) "2016")
		((= aVer 20.0) "2015")
		((= aVer 19.1) "2014")
		((= aVer 19.0) "2013")
		((= aVer 18.2) "2012")
		((= aVer 18.1) "2011")
		((= aVer 18.0) "2010")  
		((= aVer 17.2) "2009")
		((= aVer 17.1) "2008")
		((= aVer 17.0) "2007")
		((= aVer 16.2) "2006")    
		((< aVer 16.2) nil)
		(t "2020")
	)
)

(defun MT-getNumberAcadVer ( / result)
; ���������� ������ � "����������" ������� ������ autocad

	(setq result 
		(substr (getvar "ACADVER") 1 4)
	)
)

(defun MT-getAcadName ( / str key0 key1 name0 name1 value result)
; ���������� ������ � ����������������� ����� �����������
; http://www.cadforum.cz/cadforum_en/product-codes-registry-of-autodesk-cad-applications-tip6922

	(vl-load-com)
	
	(setq str (MT-getNumberAcadVer))
	(setq name0 "CurVer")
	(setq key0 (strcat "HKEY_CURRENT_USER\\Software\\Autodesk\\AutoCAD\\R" str))
	(setq value (vl-registry-read key0 name0))
	(setq key1 (strcat "HKEY_LOCAL_MACHINE\\SOFTWARE\\Autodesk\\AutoCAD\\R" str "\\" value))
	(setq name1 "ProductName")
	
	(setq result (vl-registry-read key1 name1))
)

(defun MT-getLocaleId ( / str key name value result delim pos)
; ���������� ������ � ����������������� ����� �����������
; http://www.cadforum.cz/cadforum_en/product-codes-registry-of-autodesk-cad-applications-tip6922

	(vl-load-com)
	
	(setq str (MT-getNumberAcadVer))
	(setq name "CurVer")
	(setq key (strcat "HKEY_CURRENT_USER\\Software\\Autodesk\\AutoCAD\\R" str))
	(setq value (vl-registry-read key name))
	(setq delim ":")
	(setq pos (vl-string-position (ascii delim) value))
	(setq result (substr value (+ pos 2)))
)

(defun MT-whatAcadBitVer ( / result)
; ��������� ����������� AutoCAD
; https://blog.jtbworld.com/2007/04/determine-programmatically-if-autocad.html
	
	(vl-load-com)
	(if (> (strlen (vl-prin1-to-string (vlax-get-acad-object))) 40)
		(setq result "x64")
		(setq result "x86")
	)
)

(defun MT-whatPlatformBitVer ( / result)
; ��������� ����������� OS

	(vl-load-com)
	(if (vl-string-search "64" (getenv "PROCESSOR_ARCHITECTURE"))
		(setq result "x64")
		(setq result "x86")
	)
); l��������� �� ��������� ���� �� �������� � �������

(defun MT-getCurrentDate ( / tmp year month day hh mm ss ms result)
; ������� ���������� ������ � ������� ����� � ��������
; 2012/5/7 10:29:10.56

	(setq tmp (rtos (getvar "CDATE") 2 8))
	(setq year (substr tmp 1 4))
	(setq month (substr tmp 5 2))
	(setq day (substr tmp 7 2))
	(setq hh (substr tmp 10 2))
	(setq mm (substr tmp 12 2))
	(setq ss (substr tmp 14 2))
	(setq ms (substr tmp 16))
	
	(setq result
		(strcat 
			year 
			"/" 
			month 
			"/" 
			day 
			" "
			hh 
			":"
			mm 
			":"
			ss
			"."
			ms
		)
	)
)

(defun MT-restoreScaleList ()
; ������� ������ ���������

	(vl-cmdf "_.-scalelistedit" "_r" "_y" "_e")
)

(defun MT-readFileToList (fileName / file a result)
; ������ ����� ��������� � �������� ����������� � ������

	(setq 
		file (open (findfile fileName) "r")	
		result (list) 
		a (read-line file)
	)
	(while (/= a nil)
		(setq result (append result (list a)))
		(setq a (read-line file))
	)
	(close file)
	result
)

(defun MT-writeListToFile (fileName aa clear / mode item file)
; ������ ������ � ���� ��������� aa - ������ �����
; ���������� ���� clear T

	(if clear
		(setq mode "w")
		(setq mode "a")
	)
	(setq 
		file (findfile fileName) 
		handle (open file mode)
	)
	(foreach item aa (write-line item handle))
	(close handle)
)

(defun MT-saveParamsToFile (fileName sectionName lst / comment str srcList TmpList0 tmpList1 pos item itemList result)
; ������� ��������� � ��������� ������ ������ ����������. ���� ������ �� ����� - �� ���������� ����������������
; ����������� ������������, � ������ ������� ����� ���
 	
	(vl-load-com)
	
	(setq comment ";")
		
	(setq srcList (MT-readFileToList fileName))
	(setq pos (vl-position (strcat "[" sectionName "]") srcList))
	(setq tmpList0 srcList)

	(while (>= pos 0)
		(setq tmpList1 (append tmpList1 (list (car tmpList0))))
		(setq tmpList0 (cdr tmpList0))
		(setq pos (1- pos))
	)
	(setq pos (vl-position "" tmpList0))
	(while (> pos 0)
		(setq str (car tmpList0))
		(if (= (vl-string-search comment str) 0) ; comment string
			(setq tmpList1 (append tmpList1 (list str))) ; save comment string
		)
		(setq tmpList0 (cdr tmpList0))
		(setq pos (1- pos))
	)
	(foreach x lst
		(setq item
			(strcat (car x) "=" (cdr x))
		)
		(setq itemList
			(append itemList (list item))
		)
	)
	(setq result 
		(append tmpList1 itemList tmpList0)
	)
	
	(MT-writeListToFile fileName result T)
)

(defun MT-getSectList (fileName / comment posComment fullFileName sourceList tmpList x item pos0 pos1 result)
; ������������� ������ ������ cfg-����� � ���� ������ ����� (��� [])
; ������ �����: [������]

	(setq delim "=")
	(setq comment ";")
	(if (setq fullFileName (findfile fileName))
		(progn
			(vl-load-com)
			(setq sourceList (MT-readFileToList fullFileName))
			(setq result nil)
			
		  	(mapcar
				(function
					( lambda (x)
					  	(setq x (vl-string-left-trim " " x))
						; (if (/= (vl-string-position 59 x) 0)
						(if (/= (vl-string-search comment x) 0) ; not comment string
							(progn
								(if (setq posComment (vl-string-search comment x))
									(setq x (substr x 1 posComment))
								) ; delete comment
								(setq tmpList (cons x tmpList))
							)
						)	
					)
				)
				sourceList
			); delete comments
			(foreach item tmpList
				(setq pos0 (vl-string-search "[" item))
				(setq pos1 (vl-string-search "]" item))
				(if (and (= pos0 0) (/= pos1 nil) (/= item "[]"))
					(progn
						(setq item (vl-string-trim "[]" item))
						(setq result (append result (list item)))
					)
				)
			)
		)
		(progn
			(*error* "\n���� ������������ �� ������!\n��������")
			(exit)
		)
	)
	result
)

(defun MT-getConfigList (fileName sectionName / comment posComment fullFileName sourceList tmpList pos delim item result)
; ������������� ������ cfg-����� � ���� ������ ��� (key . value)
; ������ �����: [������] -> ��������=������ ��������(��������1,��������2,��������3); �����������

	(setq delim "=")
	(setq comment ";")
	(if (setq fullFileName (findfile fileName))
		(progn
			(vl-load-com)
			(setq sourceList (MT-readFileToList fullFileName))
		  
		  	(mapcar
				(function
					( lambda (x)
					  	(setq x (vl-string-left-trim " " x))
						; (if (/= (vl-string-position 59 x) 0)
						(if (/= (vl-string-search comment x) 0) ; not comment string
							(progn
								(if (setq posComment (vl-string-search comment x))
									(setq x (substr x 1 posComment))
								) ; delete comment
								(setq tmpList (cons x tmpList))
							)
						)	
					)
				)
				sourceList
			); delete comments
		  	(setq tmpList (reverse tmpList))
		  
			(setq pos (vl-position (strcat "[" sectionName "]") tmpList))
			(while (>= pos 0)
				(setq tmpList (cdr tmpList))
				(setq pos (1- pos))
			)
			(setq pos (vl-position "" tmpList))
			(while (> pos 0)
				(setq item 
					(MT-getPairFromString
						(car tmpList)
						delim
					)
				)
				(setq tmpList (cdr tmpList))
				(setq result (append result (list item)))
				(setq pos (1- pos))
			)
		)
		(progn
			(*error* "\n���� ������������ �� ������!\n��������")
			(exit)
		); ��������
	)
	result
)

(defun MT-getValue (fileName sectName key / lst item result)
; ��������� �������� ���������

	(vl-load-com)
	
	(setq lst (MT-getConfigList fileName sectName))
	(if (setq item (assoc key lst))
		(setq result (cdr item))
		(setq result nil)
	)
)

(defun MT-getNormal2Line (P0 P1 / yy x2 y2 P2)
; ������� ���������� ������� � ������, �������� ����� ������� (P0 P1) � ����� P0
; ���������� ������ (P0 P2)
; call (getNormal2Line Pt0 Pt1)

	(setq y2 (+ (cadr P0) (- (car P0) (car P1))))
	(setq x2 (- (car P0) (- (cadr P0) (cadr P1))))
	;(setq P2 (list (car P1) yy)); X = Xp1
	(setq P2 (list x2 y2))
	(setq result (cons P0 P2))
)

(defun MT-importLine ( name fileName / fullFileName)
; ������ ����� � ����
    	
;    (if (null (tblsearch "Ltype" name)) ; - � ���������� �������
	(if (setq fullFileName (findfile fileName))
		(vl-cmdf "_-linetype" "_load" name fullFileName "")
		(progn
			(*error* "\n���� �������� ����� ����� �� ������!\n��������")
			(exit)
		); ��������
    )
)

(defun MT-createLayer (key / layerParamsList layerParams sectionName layersConfig)

	(vl-load-com)
	
	(setq layersConfig "MT_layers.cfg")
	(if (findfile layersConfig)
		(progn
			(setq sectionName "common")
			(setq layerParams (MT-getValue layersConfig sectionName key))
			(if (not layerParams)
				(setq layerParamsList nil)
					(setq layerParamsList
					(MT-stringToList 
						layerParams
						","
					)
				)
			)
		)
	)

	(if (not layerParamsList)
		(progn
			(princ "n\Layer are not create. ")
			(vl-cmdf "_layer"
				"_set" "0"
				""
			)
		)
		(if (not (tblsearch "layer" (car layerParamsList)))
			(vl-cmdf "_layer"
				"_set" "0"
				"_Make" (car layerParamsList) 
				"_LWeight" (atof (car (cdr layerParamsList))) (car layerParamsList) 
				"_Color" (atoi (cadr (cdr layerParamsList))) (car layerParamsList) 
				"_Ltype" (caddr (cdr layerParamsList)) (car layerParamsList) 
				"_Plot" (cadddr (cdr layerParamsList)) (car layerParamsList)
				"_Set" (car layerParamsList)
				""
			)
			(vl-cmdf "_layer"
				"_set" (car layerParamsList)
				""
			)
		)
	)
	
	(princ)
)

(defun MT-restoreSysvars ( / item)
; �������������� ���������� �� ������

	(if sysvarsBak
		(progn
			(foreach item sysvarsBak
				(setvar (car item) (cdr item))
			)
			(setq sysvarsBak nil)
		)
	)
	
	(princ)
)

(defun MT-help ( /  str)

	(setvar "CMDECHO" 0)
	(setq str "\n����������. ")
	(princ str)
	
	(setvar "CMDECHO" 1)
)

(defun MT-about ( / fileName lst str)

	(setvar "CMDECHO" 0)
	(setq fileName "MT_about.txt")
	(setq lst (MT-readFileToList fileName))
	(setq str "")
	(foreach x lst
		(setq str (strcat str x "\n"))
	)
	
	(alert str)
	
	(setvar "CMDECHO" 1)
)
