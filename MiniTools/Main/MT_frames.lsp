;; Copyright (c) Maks Tulin 2018
;;
;; ������� ��������� ����� � ��������� ������� � ������
;; ������� � �������������� �������� ������� �������
;;

(defun MT-insertFrame ( / dcl_id num AA blockName fileName namesList strTmp keyName)

	(setq sysvarsBak
		(list
			(cons "CMDECHO" (getvar "CMDECHO"))
			(cons "MENUECHO"(getvar "MENUECHO"))
			(cons "NOMUTT" (getvar "NOMUTT"))
			(cons "mirrtext" (getvar "mirrtext"))
			(cons "osmode" (getvar "osmode"))
		)
	); global
	
 	(vl-load-com)
	
	(setvar "cmdecho" 0)
	
	(setq strTmp " Portrait")
	(setq fileName "lib0.dwg")
	(setq namesList
		(list
			"A0"
			"A0 Portrait"
			"A1"
			"A1 Portrait"
			"A1x3"
			"A2"
			"A2 Portrait"
			"A2x3"
			"A2x4"
			"A3"
			"A3 Portrait"
			"A3x3"
			"A3x4"
			"A3x5"
			"A3x6"
			"A4 Portrait"
			"A4x3"
			"A4x4"
			"A4x5"
			"A4x6"
			"A4x7"
			"A4x8"
		)
	); ������� ������ �� ����������

	; �������� �������
	(setq dcl_id (load_dialog "MT_main.dcl"))
	(new_dialog "dwg_frame" dcl_id)
	
	(start_list "AA1")
	(mapcar 'add_list namesList)
	(end_list)

	(action_tile "AA1" "(setq num (atoi (get_tile \"AA1\")))")
	(action_tile "accept" "(setq num (atoi (get_tile \"AA1\"))) (SETQ userclick 1) (done_dialog)")
	(action_tile "cancel" "(SETQ userclick 0) (done_dialog)")

	(start_dialog)
	(unload_dialog dcl_id)

	(if (not num) ; �� ������
		(setq AA (car namesList))
		(setq AA (nth num namesList))
	)
	(princ AA)

	; �������� ���������
	(if (= userclick 1)
		(progn
			(setq keyName "_commonLay")
			(MT-createLayer keyName)
			
			(setvar "CELTYPE" "ByLayer")
			(setvar "CECOLOR" "256")
			(setvar "CELWEIGHT" -1)
			(setvar "OSMODE" 55)
			
			(setq blockName 
				(strcat "_MT_" AA)
			)
			(if (vl-string-search strTmp blockName)
				(setq blockName (vl-string-subst "v" strTmp blockName))
			)
			(if (not (tblsearch "block" blockName))
				(MT-importBlockFromLib blockName fileName)
			)
			(MT-insertBlock blockName)
		);progn
	);if
	(MT-restoreSysvars)
	(gc)
	(princ "\n���������")
	(princ)
)

(defun MT-insertStamp (2auth / blockName fileName configFileName fullFileName sectionName propertiesNamesList 
						currentPropertiesList defaultsList dcl_id key keyWin newPropertiesList saveSepStr
						keyName)
; ������� ������ �������� �������. �������������� ����������� ��� �������� ����
; �������� - ����� � 2 ������������� ��� ������� (T or nil)

	(setq sysvarsBak
		(list
			(cons "CMDECHO" (getvar "CMDECHO"))
			(cons "MENUECHO"(getvar "MENUECHO"))
			(cons "NOMUTT" (getvar "NOMUTT"))
			(cons "mirrtext" (getvar "mirrtext"))
			(cons "osmode" (getvar "osmode"))
		)
	); global
 
	(vl-load-com)
	
	(setvar "cmdecho" 0)
	
	(setq configFileName "MiniTools.cfg")
	(setq sectionName "userInfo")
	(setq propertiesNamesList 
		(list
			"Comments"
			"Keywords"
			"Subject"
			"Title"
			"_company"
			"_release_date"
			"_project_manager"
			"_standard_control"
			"_head_of_dept"
			"_inspector"
			"_author1"
			"_author2"
		)
	)
	
	(if (= (setq fullFileName (findfile "MT_dwgProperties.lsp")) nil)
		(progn
			(*error* "\n���� MT_dwgProperties.lsp �� ������!\n��������")
			(exit)
		)
	)
	(load fullFilename)
	(setq currentPropertiesList (MT-getAllDwgPropertiesList))
	(setq defaultsList (MT-getConfigList configFileName sectionName))

	(mapcar
		(function
			(lambda (x) 
				(progn
					(if (= (setq item (assoc (car x) currentPropertiesList)) nil)
						(setq currentPropertiesList (append currentPropertiesList(list x)))
						(if (= (cdr item) "")
							(progn
								(vl-remove item currentPropertiesList)
								(setq currentPropertiesList (append currentPropertiesList(list x)))
							)
						)
					)
				)
			)
		)
		defaultsList
	)

	(setq dcl_id (load_dialog "MT_stamp.dcl"))
	(new_dialog "stamp_0" dcl_id)
	
	;;���������� �������� �� ��������� (��������!!)
	(foreach x currentPropertiesList
		(setq key (car x))
		(setq keyWin (strcat key "_0"))
		(set_tile keyWin
			(vl-string-left-trim
				" "
				(cdr (assoc key currentPropertiesList))
			)
		)
	)
	
	(if 2auth
		(mode_tile "_author2_0" 0)
		(mode_tile "_author2_0" 1)
	)
	
	;;���������� ���������� ���������������� ��������
	(defun setParam ()
		(if(= (atoi (get_tile "togSepStr")) 0)
			(setq saveSepStr nil)
			(setq saveSepStr T); ���������� ��������� �� ������ (�������)
		)
		(mapcar
			(function
				(lambda (x)	
					(progn
						(setq keyWin (strcat x "_0"))
						(setq value (get_tile keyWin))
						(if (and 
								(or
									(= x "Subject")
									(= x "Comments")
								)
								(= saveSepStr T)
							)
							(setq value (MT-changeSubstr value "\\n" "\n"))
							(setq value (MT-changeSubstr value "\\n" " "))
						)
						(setq newPropertiesList 
							(append newPropertiesList
								(list
									(cons x value)
								)
							)
						)
					)
				)
			)
			propertiesNamesList
		)

	)

	(action_tile "accept" "(SETQ userclick 1) (setParam) (done_dialog)")
	(action_tile "cancel" "(SETQ userclick 0) (done_dialog)")

	(start_dialog)
	(unload_dialog dcl_id)
	
	(if (= userclick 1)
		(progn
			(MT-setAllDwgProperties newPropertiesList nil)
			
			(setq keyName "_commonLay")
			(MT-createLayer keyName)
			
			(setvar "CELTYPE" "ByLayer")
			(setvar "CECOLOR" "256")
			(setvar "CELWEIGHT" -1)
			(setvar "OSMODE" 55)
			
			(setq fileName "lib0.dwg")
			(if 2auth
				(setq blockName "_MT_ST_3_2AUTH")
				(setq blockName "_MT_ST_3")
			)
			(if (not (tblsearch "block" blockName))
				(MT-importBlockFromLib blockName fileName)
			)
			(MT-insertBlock blockName)
			(vl-cmdf "_regenall")
		)
	)
	(MT-restoreSysvars)
	(gc)
	(terpri)
	(princ "\n���������")
	(princ)
)

(defun MT-insertStampOther (name / blockName fileName keyName)
; ������� ������� ������� � ����������
; �������� - ��� ������
; ����� (MT-insertStampOther "_SG")

	(setq sysvarsBak
		(list
			(cons "CMDECHO" (getvar "CMDECHO"))
			(cons "MENUECHO"(getvar "MENUECHO"))
			(cons "NOMUTT" (getvar "NOMUTT"))
			(cons "mirrtext" (getvar "mirrtext"))
			(cons "osmode" (getvar "osmode"))
		)
	); global
	
	(setvar "cmdecho" 0)
	
	(setq keyName "_commonLay")
	(MT-createLayer keyName)
	
	(setvar "CELTYPE" "ByLayer")
	(setvar "CECOLOR" "256")
	(setvar "CELWEIGHT" -1)
	(setvar "OSMODE" 55)
	
	(setq fileName "lib0.dwg")
	(setq blockName (strcat "_MT_ST" name))
	(if (not (tblsearch "block" blockName))
		(MT-importBlockFromLib blockName fileName)
	)
	(MT-insertBlock blockName)
	(MT-restoreSysvars)
	(gc)
	(terpri)
	(princ "\n���������")
	(princ)
)

(defun MT-editStamp ( / fullFilename propertiesNamesList dcl_id key val keyWin oldPropertiesList newPropertiesList saveSepStr)
; ��������� ������� DWG- ����� (�, ��������������, ����������� � ���� ������ �������� �������)
; ��� ��� ��������� �������� ���� ������ ������� - ����� �� �����	
	
		(setq sysvarsBak
		(list
			(cons "CMDECHO" (getvar "CMDECHO"))
			(cons "MENUECHO"(getvar "MENUECHO"))
			(cons "NOMUTT" (getvar "NOMUTT"))
			(cons "mirrtext" (getvar "mirrtext"))
			(cons "osmode" (getvar "osmode"))
		)
	); global
 
	(vl-load-com)
	
	(setvar "cmdecho" 0)
	
	(setq propertiesNamesList 
		(list
			"Comments"
			"Keywords"
			"Subject"
			"Title"
			"_company"
			"_release_date"
			"_project_manager"
			"_standard_control"
			"_head_of_dept"
			"_inspector"
			"_author1"
			"_author2"
		)
	)
	
	(if (= (setq fullFilename (findfile "MT_dwgProperties.lsp")) nil)
		(progn
			(*error* "\n���� MT_dwgProperties.lsp �� ������!\n��������")
			(exit)
		)
	)
	(load fullFilename)
	(setq oldPropertiesList (MT-getAllDwgPropertiesList))
	
	(setq dcl_id (load_dialog "MT_stamp.dcl"))
	(new_dialog "stamp_0" dcl_id)
	
	;;���������� �������� �� ���������
	(foreach x oldPropertiesList
		(setq key (car x))
		(setq val (cdr (assoc key oldPropertiesList)))
		(setq val (MT-changeSubstr val "\n" "\\n"))
		(setq keyWin (strcat key "_0"))
		(set_tile keyWin
			(vl-string-left-trim " " val)
		)
	)
	(set_tile "togSepStr" "1")
	
	;;���������� ���������� ���������������� ��������
	(defun setParam ()
		(if(= (atoi (get_tile "togSepStr")) 0)
			(setq saveSepStr nil)
			(setq saveSepStr T); ���������� ��������� �� ������ (�������)
		)
		(mapcar
			(function
				(lambda (x)	
					(progn
						(setq keyWin (strcat x "_0"))
						(setq value (get_tile keyWin))
						(if (and 
								(or
									(= x "Subject")
									(= x "Comments")
								)
								(= saveSepStr T)
							)
							(setq value (MT-changeSubstr value "\\n" "\n"))
							(setq value (MT-changeSubstr value "\\n" " "))
						)
					  	(setq newPropertiesList 
							(append newPropertiesList
								(list
									(cons x value)
								)
							)
						)
					  	;(princ (cons x value))
					)
				)
			)
			propertiesNamesList
		)
	)

	(action_tile "accept" "(SETQ userclick 1) (setParam) (done_dialog)")
	(action_tile "cancel" "(SETQ userclick 0) (done_dialog)")

	(start_dialog)
	(unload_dialog dcl_id)
	
	(if (= userclick 1)
		(progn
			(MT-setAllDwgProperties newPropertiesList nil)
			(vl-cmdf "_regenall")
		)
	)
	
	(MT-restoreSysvars)
	(gc)
	(terpri)
	(princ "\n���������")
	(princ)
)

(defun MT-insertLogo( / configFileName sectionName path fileName srcFileName blockName dstBlockName acadDoc odbx lst dcl_id st str oldEcho oldLayer cnt keyName)
; ������� ��� ���������� �������� � ������

	(setq oldEcho (getvar "cmdecho"))
	(setvar "cmdecho" 0)
	
	(setq oldLayer (getvar "CLAYER"))
	
	(setq configFileName 
		(findfile 
			"MiniTools.cfg"
		)
	)
	(setq sectionName "main")
	(setq path 
		(MT-getValue configFileName sectionName "_libsPath")
	)
	(if (or (= path "") (= path nil))
		(setq path
			(strcat
				(MT-getValue configFileName sectionName "_path")
				"\\MiniTools\\Plugins\\lib"
			)
		)
	)
	(setq path (strcat path "\\"))
	(setq fileName
		(findfile (strcat path "lib_logo.dwg"))
	)
	(if (/= fileName nil)
		(progn
			(setq cnt T)
			(vl-load-com)
			(setq acadDoc (vla-get-activedocument (vlax-get-acad-object)))
			(setq lst (MT-getCommentsBlockInLib fileName))
			
			(while cnt
			
				(setq dcl_id (load_dialog "MT_main.dcl"))
				(new_dialog "selection_list" dcl_id)

				(start_list "A1")
				(mapcar 'add_list lst)
				(end_list)

				(action_tile "A1" "(setq st (get_tile \"A1\"))")
				(action_tile "accept" "(setq st (get_tile \"A1\")) (SETQ userclick 1) (done_dialog)")
				(action_tile "cancel" "(SETQ userclick 0) (done_dialog)")

				(start_dialog)
				(unload_dialog dcl_id)
				
				(if (and 
						(= st "")
						(= userclick 1)
					)
					(progn
						(princ "\n������ ������! ")
						(SETQ userclick 0)
					)

				)

				(if (= userclick 1)
					(progn
						(setq str (nth (atoi st) lst))
						(princ str)
						
						(setq blockName (MT-getBlockNameIfComment fileName str))
						(setq dstBlockName "_MT_logo")
						(setq srcFileName
							(strcat 
								(getvar "TEMPPREFIX")
								dstBlockName
								".dwg"
							)
						)
						
						(MT-exportBlockFromLib blockName fileName srcFileName)
						
						(setq keyName "_logoLay")
						(MT-createLayer keyName)
						(setvar "celtype" "ByLayer")
						(setvar "cecolor" "256")
						
						(vl-cmdf "_.-insert" (strcat dstBlockName "=" srcFileName) "_y")
						(vl-cmdf)

						(vl-file-delete srcFileName)
						
						(setq cnt nil)
					)
					(setq cnt nil)
				)
			)
		)
		(princ "�� ������� ���������� ���������!")
	)
	
	(setvar "CLAYER" oldLayer)
	(setvar "cmdecho" oldEcho)
	(gc)
	(princ)
	(princ "\n���������")
	(princ)
)

(load (findfile "MT_main.lsp"))
