od :dialog {label="���� ����� ������";
      :row {
      :boxed_column
              {label="�������";
               fixed_width=true;
               width=15;
       :toggle   {label= "��������� ��������";
                  key="AA";
                  fixed_width = true;
                 }
       :toggle   {label= "��������� ����������";
                  key="BB";
                  fixed_width = true;
                 }
       :toggle   {label= "��������� ����������";
                  key="SS";
                  fixed_width = true;
                 }
              }
      :boxed_column
              {
               fixed_width=true;
               width=15;
       :toggle   {label= "��������� ����������";
                  key="FF";
                  fixed_width = true;
                 }
       :toggle   {label= "����� ���";
                  key="HH";
                  fixed_width = true;
                 }
       :text {key="odtext";
                 value="�������� \n����������� \n��������.";
                 width=15;
                 height=5;
                 }
              } 
           }
ok_cancel;
}

tbl_blk :dialog {label="��� �������";
	: boxed_radio_column {
		fixed_width=true;
		width=15;
		:radio_button {
			key = AA;
			label = "�������";
			value = "1";
		}
		:radio_button {
			key = BB;
			label = "����";
		}
	}
ok_cancel;
}

otm :dialog {label="�������";
      :row {
      :boxed_column{
               fixed_width=true;
               width=15;
      :popup_list {label= "M������, 1:";
                 key="A1";
                 edit_width=7;
                 height=5;
                 value = "1";
                  }
               }
      :boxed_column{
               fixed_width=true;
               width=15;
       :toggle   {label= "�� ���. ������";
                  key="RR";
                  fixed_width = true;
                  value = "1";
                 }
       :toggle   {label= "�� �����";
                  key="FF";
                  fixed_width = true;
                 }
               }
           }
ok_cancel;
}

otv :dialog {label="��������� �������������";
      :row {
      :boxed_column
              {label="���������";
               fixed_width=true;
               width=20;
       :edit_box {label="������, �� (X)";
                  key="A1";
                  edit_width=8;
                 }
       :edit_box {label="������, �� (Y)";
                  key="B1";
                  edit_width=8;
                 }
              }
           }
ok_cancel;
}

poz :dialog {label="�������";
      :row {
      :boxed_column{
       :edit_box {label="�������";
                  key="A1";
                  edit_width=6;
                 }
       :edit_box {label="����������";
                  key="B1";
                  edit_width=6;
                 }
             }
          }
ok_cancel;
}

Welding :dialog {label = "�������� ��� ���";
           : row{
              : boxed_radio_column {
                fixed_width=true;
                   :radio_button {
                    key = AA1;
                    label = "���������";
                    value = "1";
                    }
                  fixed_width=true;
                  width=15;
                    :radio_button {
                     key = AA2;
                     label = "���������";
                    }
               }
              : boxed_radio_column {
                fixed_width=true;
                   :radio_button {
                    key = BB1;
                    label = "��������";
                    value = "1";
                    }
                  fixed_width=true;
                  width=15;
                    :radio_button {
                     key = BB2;
                     label = "�������";
                    }
               }
              : boxed_radio_column {
                fixed_width=true;
                   :radio_button {
                    key = CC1;
                    label = "��������";
                    value = "1";
                    }
                  fixed_width=true;
                  width=15;
                    :radio_button {
                     key = CC2;
                     label = "�����������";
                    }
               }
           }
           :boxed_row {
ok_cancel;
           }
}

dwg_frame :dialog {label = "����� �������";
	:row {
		:popup_list {label = "������: ";
			key = "AA1";
			edit_width = 10;
			height = 5;
			value = "";
		}
	}
	:row {
		ok_cancel;
	}
}

axis :dialog {label = "��������� ���";
      :row {
      :boxed_column
              {label= "������";
               fixed_width=true;
               width=15;
       :edit_box {label= "�� ������ �����";
                  key= "BB1";
                  edit_width=5;
                 }
       :edit_box {label= "�� ������ �����";
                  key= "BB2";
                  edit_width=5;
                 }
              }
      :boxed_column
              {
               fixed_width=true;
               width=15;
      :popup_list {label= "M������, 1:";
                 key="A1";
                 edit_width=5;
                 height=5;
                 value = 3;
                  }
       :toggle   {label= "���������";
                  key="FF";
                  fixed_width = true;
                 }
              }
           }
      :row {
      :text    {key="osstext";
                 value="��� ���������� �� ����� ������� ������� 1:1.";
               }
           }

ok_cancel;
}

m_axis :dialog {label = "����� ����";
      :row {
      :boxed_column
              {label = "���";
               fixed_width=true;
               width=8;
       :edit_box {label="�� �����������";
                  key="SS1";
                  edit_width=6;
                 }
       :edit_box {label="�� ���������";
                  key="SS2";
                  edit_width=6;
                 }
              }
      :boxed_column
              {label = "�������";
               fixed_width=true;
               width=8;
       :edit_box {label= "�� �����������";
                  key="RR1";
                  edit_width=6;
                 }
       :edit_box {label= "�� ���������";
                  key="RR2";
                  edit_width=6;
                 }
              }
           }
       
      :boxed_row {
      :popup_list {label= "M������ ����������� �� �����, 1:";
                 key="A1";
                 edit_width=8;
                 value = 3;
                  }
           }
ok_cancel;
}

axis_mark :dialog {label = "������ ���";
      :row {
      :boxed_column
            {
      :row   {
      :popup_list {label = "M������ �����������, 1:";
                 key="A1";
                 edit_width=5;
                 height=5;
                 value = 3;
                  }
            }
      :row  {
      :text {key="ospodtext";
                 value="��� ���������� �� ����� \n������� ������� 1:1.";
                 width=15;
                 height=2;}
            }
           }
          }
ok_cancel;
}


axis_gp :dialog {label = "��� �� ��";
      :row {
      :boxed_column
            {
      :row   {
      :popup_list {label = "M������ �����������, 1:";
                 key="A1";
                 edit_width=5;
                 height=5;
                 value = 2;
                  }
            }
      :row  {
      :text {key="ospodtext";
                 value="��� ���������� �� ����� \n������� ������� 1:1.";
                 width=15;
                 height=2;}
            }
           }
          }
ok_cancel;
}


coord :dialog {label="����������";
	:row   {
		:popup_list {
			label= "M������, 1:";
			key="AA";
                 	edit_width=8;
                 	height=5;
                 	value = 1;
		}
	}
	:row  {
		:toggle   {
			label= "�� �����";
                  	key="FF";
                  	fixed_width = true;
		}
	}
ok_cancel;
}

CHMENU :dialog {label = "����� ������ ����";
	:row  {
		:boxed_column {
			fixed_width=true;
			width=15;
			:popup_list {
				key = "AA1";
				height=5;
				value = "";
			}
			:toggle   {
			label= "���������";
                  	key="FF";
                  	fixed_width = true;
			}
		}
		:boxed_column {
			fixed_width=true;
			width=15;
			:text {
				value = "�������� <<MT-�����>>\n����� ���������\n���. ����";
				height=5;
			}
		}
	}
ok_cancel;
}

common_mark :dialog {label = "���������";
      :row {
      :boxed_column
            {
      :row   {
      :popup_list {label = "M������ �����������, 1:";
                 key="A1";
                 edit_width=5;
                 height=5;
                 value = 3;
                  }
            }
      :row  {
      :text {key="ospodtext";
                 value="��� ���������� �� ����� \n������� ������� 1:1.";
                 width=15;
                 height=2;}
            }
           }
          }
ok_cancel;
}

select_mark :dialog {label = "�����";
      :row {
      :boxed_column
            {
      :popup_list {label = "�����:";
                 key="A1";
                 edit_width=15;
                 height=15;
                 value = 1;
                  }
            }
          }
ok_cancel;
}

selection_list :dialog {label = "����� ��������";
      :row {
      :boxed_column
              {
               fixed_width=true;
               width=40;
      :list_box  {label = "��������:";
                 key="A1";
                 edit_width=35;
                 height=15;
                 }
              }
           }
ok_cancel;
}
