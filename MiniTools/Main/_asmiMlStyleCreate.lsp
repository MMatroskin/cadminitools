;; Creating MLINESTYLE
;; 
;; original:
;; https://www.cadtutor.net/forum/topic/3557-multiline/?do=findComment&comment=28606
;; add for example "6_PLINES" style with 6 lines:
;; Command: (asmi-mlStyleCreate 6)
;; 

(defun asmi-mlStyleCreate(Quont / dxfLst topOrd Count mlDict)
	(setq dxfLst
	(list'(0 . "MLINESTYLE")'(102 . "{ACAD_REACTORS")'(102 . "}")
	'(100 . "AcDbMlineStyle")(cons 2(strcat(itoa Quont)"_PLINES"))
	'(70 . 0)'(3 . "")'(62 . 256)'(51 . 1.5708)'(52 . 1.5708)
	(cons 71 Quont))
		Count 0.0
		topOrd(-(/ Quont 2.0) 0.5)
	); end setq
	(repeat Quont
		(setq dxfLst
			(append dxfLst
				(list(cons 49(- topOrd Count))
					'(62 . 256) '(6 . "BYLAYER")
				)
			)
			Count(1+ Count)
	   );end setq
   ); end repeat
   (if (null
			(member
				(assoc 2 dxfLst)(dictsearch(namedobjdict)"ACAD_MLINESTYLE")
			)
		)
		(progn
            (setq mlDict
				(cdr
					(assoc -1
						(dictsearch(namedobjdict)"ACAD_MLINESTYLE")
					)
				)
			)
            (dictadd mlDict(cdr(assoc 2 dxfLst))(entmakex dxfLst))
		); end progn
    ); end if
    (strcat(itoa Quont)"_PLINES")
); end of asmi-mlStyleCreate
