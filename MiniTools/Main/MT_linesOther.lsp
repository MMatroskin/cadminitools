;; Copyright (c) Maks Tulin 2008-2011/2018
;;
;; ������� ��� ��������� �����
;;

(defun MT-zemLine (MY-LT / ltype-file)
;; ����������� ����� ����� �����

    (setq ltype-file "MT_lines")
    (if (null (tblsearch "Ltype" MY-LT))
        (command "_-linetype" "_load" MY-LT ltype-file "")
    )
    (if (tblsearch "layer" "025_common")
      	(command "_layer" "_Set" "025_common" "")
      	(command "_layer" "_set" "0" "_Make" "025_common" "_LWeight" "0.25" "025_common" "_Color" "7" "025_common" "_set" "025_common" "")
    )
    (setvar "celtype" MY-LT)
    (setvar "cecolor" "256")
    (setvar "CELWEIGHT" -1)
    (setvar "osmode" 1015)
    (command "_.pline")
    (while (= 1 (getvar "CMDACTIVE"))
        (command pause)
    ); end while
    (setvar "celtype" "ByLayer")
	(terpri)
	(princ "\n���������")
	(princ)
)
