;; Copyright (c) Maks Tulin 2007-2011/2018
;;
;; ������� ��������� ������ 
;;

(defun MT-insPR (blockName / P0 N1 fileName keyName)
; ������� ����� �������� / ��������

	(setvar "celtype" "ByLayer")
	(setvar "cecolor" "254")
	(setvar "CELWEIGHT" 18)
	(setvar "osmode" 0)
	
	(princ)
	(setq P0 (getpoint "\n������� ����� �������"))
	(princ)
	(if (= P0 nil)
		(setq N1 1) (setq N1 0)
	)
	
	(setq keyName "_commonLay")
	(MT-createLayer keyName)

	(while (= N1 0)
		(setq fileName "lib1.dwg")
		(if (not (tblsearch "block" blockName))
			(MT-importBlockFromLib blockName fileName)
		)
		
		(command "_.insert" blockName P0 1 1 0)
		(while (= 1 (getvar "CMDACTIVE"))
			(command pause)
		); end while
		(setq P0 (getpoint "\n������� ����� �������"))
		(princ)
		(if (= P0 nil)
			(setq N1 (1+ N1)) (setq N1 0)
		)
		(princ)
	)
	(setvar "cecolor" "256")
	(setvar "CELWEIGHT" -1)
	(princ "\n���������")
	(princ)
)

(defun MT-hole01 ( / A B N1 blockName fileName P0 userclick dcl_id keyName)
; ��������� ������������� - ����������
; ������� �������, ����� ��������� (�������� � ������, ������� � ����� �����������???)

	(setvar "attdia" 1)
	(setvar "osmode" 1015)

	;��������� �������� ����������
	(setq A 100)
	(setq B 100)
	
	(setvar "celtype" "ByLayer")
	(setvar "cecolor" "256")
	(setvar "CELWEIGHT" -1)
	
	(setq N1 T)
	(while N1

		;�������� �������
		(setq dcl_id (load_dialog "MT_main.dcl"))
		(new_dialog "otv" dcl_id)
		;;���������� �������� �� ���������
		(set_tile "A1" (rtos A))
		(set_tile "B1" (rtos B))
		;;���������� ���������� ���������������� ��������
		(defun sper (/)
			(setq A (atof (get_tile "A1")))
			(setq B (atof (get_tile "B1")))
		)

		(action_tile "accept" "(SETQ userclick 1) (sper) (done_dialog)")
		(action_tile "cancel" "(SETQ userclick 0) (done_dialog)")

		(start_dialog)
		(unload_dialog dcl_id)
		(IF (= userclick 1)
			(PROGN
				(setq keyName "_holeLay")
				(MT-createLayer keyName)

				(setq blockName "otv-1")
				(setq fileName "lib1.dwg")
				(if (not (tblsearch "block" blockName))
					(MT-importBlockFromLib blockName fileName)
				)
				
				(setq P0 (getpoint "\n������� ����� ��������� <����� >: "))
				(princ)
				(while (/= P0 nil)
					(progn
						(vl-cmdf "_.-insert" blockName P0 A B 0)
						(setq P0 (getpoint "\n������� ����� ��������� <����� >: "))
						(princ)
					)
				)
			)
			(setq N1 nil)
		)
	)
	(princ "\n���������")
	(princ)
)

(defun MT-hole02 ( / D blockName fileName keyName)
; ��������� ������� - �������� �������?

	(setvar "celtype" "ByLayer")
	(setvar "cecolor" "256")
	(setvar "CELWEIGHT" -1)
	
	(setq D (getint "\n������� ������� ��������� � �� <�����>:"))
	(if (= D nil)
		(setq N1 1) (setq N1 0)
	)
	
	(while (= N1 0)
		(setq keyName "_holeLay")
		(MT-createLayer keyName)
		
		(setq blockName "otv-2")
		(setq fileName "lib1.dwg")
		(if (not (tblsearch "block" blockName))
			(MT-importBlockFromLib blockName fileName)
		)
		(setvar "osmode" 1015)

		(vl-cmdf "_.-insert" blockName pause D D 0)
		
		(setq D (getint "\n������� ������� ��������� � �� <�����>:"))
		(if (= D nil)
			(setq N1 (1+ N1))
		)
		(princ)
	)
	(princ "\n���������")
	(princ)
)

(defun MT-levelPlan ( / P0 N1 blockName fileName keyName oldOsmode)
; ������� ����� ������� �� ����

	(setq oldOsmode (getvar "osmode"))
	(setvar "celtype" "ByLayer")
	(setvar "cecolor" "256")
	(setvar "CELWEIGHT" -1)
	(setvar "osmode" 0)
	(princ)
	(setq P0 (getpoint "\n������� ����� �������"))
	(princ)
	(if (= P0 nil)
		(setq N1 1) (setq N1 0)
	)
	(princ)
	
	
	(setq keyName "_commonLay")
	(MT-createLayer keyName)
		
	(while (= N1 0)
		(setq blockName "OTMPL")
		(setq fileName "lib1.dwg")
		(if (not (tblsearch "block" blockName))
			(MT-importBlockFromLib blockName fileName)
		)
		
		(MT-insertBlock (list blockName P0))
		(setq P0 (getpoint "\n������� ����� �������"))
		(princ)
		(if (= P0 nil)
			(setq N1 (1+ N1)) (setq N1 0)
		)
		(princ)
	)
	
	(setvar "osmode" oldOsmode)
	(princ "\n���������")
	(princ)
)

(defun MT-markOrient (blockName / oldOsmode fileName keyName)
; ������� ����� ������ ��� ���� ( "_UKL" "VID")
; ����� (MT-markOrient "_UKL"), (MT-markOrient "VID")
	
	(setq oldOsmode (getvar "osmode"))
	(setvar "celtype" "ByLayer")
	(setvar "cecolor" "256")
	(setvar "CELWEIGHT" -1)
	
	(setq keyName "_commonLay")
	(MT-createLayer keyName)

	(setq fileName "lib1.dwg")
	(if (not (tblsearch "block" blockName))
		(MT-importBlockFromLib blockName fileName)
	)
	(MT-insertOrientBlock blockName)
	(setvar "osmode" oldOsmode)
	(princ "\n���������")
	(princ)
)

(defun MT-uzlMark (/ blockName b_name1 fileName QWE P0 N1 oldOsmode keyName)
; ����� ����

	(setq oldOsmode (getvar "osmode"))
	(setvar "attdia" 1)
	
	(vl-load-com)
	
	(setq keyName "_commonLay")
	(MT-createLayer keyName)
	
	(setq fileName "lib1.dwg")
	(setq QWE (getstring (prompt "\n������ ������ �� ����? Y/N <Y>: ")))
	(if (or (= QWE "y") (= QWE ""))
		(setq blockName "UZL-MK1")
		(setq blockName "UZL-MK2")
	)
	(setvar "osmode" 55);
	(setq P0 (getpoint "\n������� ����� ������� <������>"))
	(princ)
	(if (= P0 nil)
		(setq N1 1) (setq N1 0)
	)
	(while (= N1 0)
		(if (not (tblsearch "block" blockName))
			(MT-importBlockFromLib blockName fileName)
		)
		(setq b_name1 (list blockName P0))
		(MT-insertBlock b_name1)
		(princ)
		(setq P0 (getpoint "\n������� ����� ������� <������>"))
		(princ)
		(if (= P0 nil)
			(setq N1 (1+ N1)) (setq N1 0)
		)
		(princ)
	); while

	(setvar "osmode" oldOsmode)
	(princ "\n���������")
	(princ)
)

(defun MT-view (/ P1 P2 N1 blockName fileName layerName oldEcho oldOsmode)
;; ����������� ���� (�������� - ��������� � �������� ������ (MT-markOrient))

   (setq oldEcho (getvar "cmdecho"))
   (setvar "cmdecho" 0)
   (setq oldOsmode (getvar "osmode"))
   
   (setvar "attdia" 1)
   (setvar "mirrtext" 0)
   (setq layerName "_025_common")
	(if (tblsearch "layer" layerName)
		(command "_layer" "_Set" layerName "")
		(command "_layer" "_set" "0"
			 "_make" layerName
			 "_color" "7" layerName
			 "_LWeight" "0.25" layerName
			 "_set" layerName "")
	);�������� ������� ���� "MARK" � ��� �������� � ������ ����������
   (setvar "celtype" "ByLayer")
   (setvar "cecolor" "256")
   (setvar "CELWEIGHT" -1)
   (setvar "osmode" 1015)
   (setq P1 (getpoint "\n������� ����� ������� �������: "))
   (princ)
   (if (= P1 nil)
		(setq N1 1) (setq N1 0)
   )
   (while (= N1 0)
		 (command "_ortho" "_on")
		 (setvar "osmode" 0)
		 (setq P2 (getpoint P1 "\n������� �����������: "))
		 (terpri)
		 
		 (if (or (> (cadr P2) (cadr P1)) (< (cadr P2) (cadr P1)))
			  (setq ANG 90)
			  (setq ANG 0)
		 )
		 (setq fileName "lib1.dwg")
		 (setq blockName "VID")
		 (if (not (tblsearch "block" blockName))
			(MT-importBlockFromLib blockName fileName)
		 )
		 (MT-insertBlock (list blockName P1 ANG))
		 (if (< (cadr P2) (cadr P1))
				 (command "_mirror" (entlast) "" P1 (list (+ (car P1) 100) (cadr P1)) "_y")
		 )
		 (if (< (car P2) (car P1))
				 (command "_mirror" (entlast) "" P1 (list (car P1) (+ (cadr P1) 100)) "_y")
		 )
		 (setvar "osmode" 1015)
		 (command "_ortho" "_off")
		 (setq P1 (getpoint "\n������� ����� ������� �������: "))
		 (princ)
		 (if (= P1 nil)
			(setq N1 (1+ N1)) (setq N1 0)
		 )
		 (princ)
	)
	(setvar "osmode" oldOsmode)
	(princ "\n���������")
	(princ)
)

(defun MT-viewMark ( / blockName layerName height P0 N NN oldOsmode)
; ������� ����
	
	(setq oldOsmode (getvar "osmode"))
	(setq blockName "_viewMark")
	
	(if (not (tblsearch "block" blockName))
		(progn
			(vl-cmdf "_layer" "_set" "0" "")
			
			(setvar "celtype" "Continuous")
			(setvar "cecolor" "0")
			(setvar "CELWEIGHT" -2)

			(setq height 3.5); ������ ������
			
			(setq  NN  (ssadd))
			(vl-cmdf "_AFLAGS" 20)
			(vl-cmdf "_-attdef" "" "TITLE" "Title" "" "_J" "_BL" '(1 0) height 0)
			(ssadd (entlast) NN)
			(vl-cmdf "_-Text" "_J" "_BR" '(0 0) height 0 "���")
			(ssadd (entlast) NN)
			
			(vl-cmdf "_-BLOCK" blockName "0,0" NN "")
		)
	)
	
	(setq keyName "_commonLay")
	(MT-createLayer keyName)

	(setvar "celtype" "ByLayer")
	(setvar "cecolor" "256")
	(setvar "CELWEIGHT" -1)
	(setvar "osmode" 55)
	
	(setq P0 (getpoint "\n������� ����� ������� <�����>: "))
	(princ)
	(if (= P0 nil)
		(setq N 1)
		(setq N 0)
	)
	(while (= N 0)
		(MT-insertBlock (list blockName P0))
		(setq P0 (getpoint "\n������� ����� ������� <�����>: "))
		(princ)
		(if (= P0 nil)
			(setq N 1)
		)
	)
	(setvar "osmode" oldOsmode)
	(princ "\n���������")
	(princ)
)

(defun MT-sectMark ( / blockName height P0 N NN val valTmp qwe item keyName oldOsmode)
; ������� �������

	(setq oldOsmode (getvar "osmode"))
	(setq blockName "_sectMark")
	
	(if (not (tblsearch "block" blockName))
		(progn
			(vl-cmdf "_layer" "_set" "0" "")
			
			(setvar "celtype" "Continuous")
			(setvar "cecolor" "0")
			(setvar "CELWEIGHT" -2)

			(setq height 3.5); ������ ������
			
			(setq  NN  (ssadd))
			(setvar "AFLAGS" 20)
			(vl-cmdf "_-attdef" "" "TITLE" "Title" "1" "_J" "_BR" '(-2 0 0) height 0)
			(ssadd (entlast) NN)
			(vl-cmdf "_-attdef" "" "FIELD" "Do not change this TAG" "1" "_J" "_BL" '(2 0 0) height 0)
			(ssadd (entlast) NN)
			(vl-cmdf "_Line" "-1,2.5,0" "1,2.5,0" "")
			(ssadd (entlast) NN)
			
			(vl-cmdf "_-BLOCK" blockName "0,0,0" NN "")
		)
	)
	
	(setq keyName "_commonLay")
	(MT-createLayer keyName)

	(setvar "celtype" "ByLayer")
	(setvar "cecolor" "256")
	(setvar "CELWEIGHT" -1)
	(setvar "osmode" 55)
	(setvar "attreq" 0)
	
	(setq qwe (strcat "\n����� ������� <�����>: "))
	(setq val (getint qwe))
	(princ)

	(while val
		(vl-cmdf "_.-insert" blockName pause 1 1 0)
		(setq item (entlast))
		(MT-editAttributeInBlock item "TITLE" (itoa val)); ��������� ��������1
		(setq str (MT-getNewFieldValue item "TITLE"))
		(MT-editAttributeInBlock item "FIELD" str) ; ��������� ���� � ��������2
		(vl-cmdf "_REGENALL")

		(setq val (getint qwe))
	)
	(setvar "osmode" oldOsmode)
	(setvar "attreq" 1)
	(princ "\n���������")
	(princ)
)

(load (findfile "MT_main.lsp"))
