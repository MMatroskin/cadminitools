;; Copyright (c) Maks Tulin 2018
;;
;; creating styles for text, dimensions
;; tables??? mLines???


(defun MT-createTextStyle (stylePropertyList  / lst styleName fontName wdh ang res)
; create text style
; call: (MT-createTextStyle ("GOST_2.304" "romans" 0.8 0))
; parametr (style name, font name, weight, angle)
    
	(setq styleName (car stylePropertyList))
	(setq lst (cdr stylePropertyList))
	(setq fontName (car lst))
	(setq wdh (cadr lst))
	(setq angRad 
		(/ 
			(* 
				pi 
				(caddr lst)
			) 
			180
		)
	)
	
	(entmakex
		(list 
			'(0 . "STYLE")
			'(100 . "AcDbSymbolTableRecord")
			'(100 . "AcDbTextStyleTableRecord")
			(cons 2 styleName)
			(cons 3 fontName)
			(cons 40 0)
			(cons 41 wdh)
			(cons 42 2.5); Last height used
			(cons 50 angRad)
			(cons 70 0)
		)
	)

)

(defun MT-createDimStyle (stylePropertyList / styleName lst blk entBlk blkBackup 
											textStyleName round pres hgt shortLines)

; create dimension style
; short dim lines = T - not work now
; https://knowledge.autodesk.com/ru/support/autocad/learn-explore/caas/CloudHelp/cloudhelp/2019/RUS/AutoCAD-Core/files/GUID-83E8F93F-A715-4066-95A1-9D1C49398507-htm.html
; round  - MT-common = 1.0, MT-OGP = 0.01
; hgt - MT-common = 2.5, MT-OGP = 2.0
; pres - MT-common = 0, MT-OGP = 2
; parametr: (style name, text style name, height, roundof, precision, short dim lines)
;
; call: (MT-createDimStyle ("MT-common" "GOST_2.304" 2.5 1 0 Nil))

	(setq styleName (car stylePropertyList))
	(setq lst (cdr stylePropertyList))
	(setq textStyleName (car lst))
	(setq hgt (cadr lst))
	(setq round (caddr lst))
	(setq pres (cadddr lst))
	(setq shortLines (cadr (cdddr lst)))
	
	;(setq blkBackup (getvar "dimblk"))
	(setq blk "_ArchTick")
	(setvar "dimblk" blk)
	;(setq entBlk (tblobjname "block" (strcat "_" (getvar "dimblk")))); локализация !!!!
	(setq entBlk (tblobjname "block" blk))
	; (if (= blkBackup "")
		; (setvar "dimblk" ".")
		; (if (tblobjname "block" blkBackup)
			; (setvar "dimblk" (strcat "_" blkBackup))
		; )
	; )
	(setvar "dimblk" ".")

	(setq propList
		(list
            '(0 . "DIMSTYLE")
            '(100 . "AcDbSymbolTableRecord")
            '(100 . "AcDbDimStyleTableRecord")
            (cons 2 styleName)
            (cons 70 0)
            (cons 40 0.0) ;dimscale
            (cons 41 2.0) ;dimasz   
            (cons 42 1.25) ;dimexo  
            (cons 43 8.0) ;dimdli   
            (cons 44 1.25) ;dimexe   
            (cons 45 round) ;dimrnd  
            (cons 46 1.25) ;dimdle 
			(cons 49 15.0) ; DIMFXL - lenght dimensions lines ???
            (cons 72 0) ;dimlim
            (cons 73 0) ;dimtih  
            (cons 74 0) ;dimtoh  
            (cons 77 1) ;dimtad  
            (cons 78 0) ;dimzin - !!!
            (cons 79 2) ;dimazin
			(cons 140 hgt) ;dimtxt   
            (cons 141 2.5) ;dimcen  
            (cons 143 0.0394) ;dimaltf
            (cons 145 0) ;dimtvp
            (cons 147 0.5) ;dimgap
            (cons 170 0) ;dimalt
            (cons 172 1) ;dimtofl
            (cons 173 0) ;dimsah   
            (cons 174 0) ;dimtix
            (cons 175 0) ;dimsoxd (if dimtix)
            (cons 176 0) ;dimclrd
            (cons 177 0) ;dimclre
            (cons 178 0) ;dimclrt
            (cons 179 1) ;dimadec
            (cons 271 pres) ;dimdec   
            (cons 272 0) ;dimtdec
            (cons 275 1) ;dimaunit
            (cons 277 2) ;dimlunit
            (cons 278 44) ;dimdsep 
            (cons 279 1) ;dimtmove
            (cons 280 0) ;dimjust
            (cons 281 0) ;dimsd1
            (cons 282 0) ;dimsd2
            (cons 283 0) ;dimtolj
            (cons 284 8) ;dimtzin
            (cons 288 0) ;dimupt
            (cons 340 (tblobjname "style" textStyleName)) ;DIMTXTSTY
			(cons
				342
				(cdr (assoc 330 (entget entBlk)))
			) 
			(cons 371 -2) ;dimlwd
            (cons 372 -2) ;dimlwe
        )
	)
	; (if shortLines 
		; (setvar "DIMFXLON" 1)
		; (setvar "DIMFXLON" 0)
	; ); WTF ?????

	(entmakex propList)

)
