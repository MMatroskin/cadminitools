;; Copyright (c) Maks Tulin 2008-2011/2018
;;
;; ��������� ��������� �������.
;; �������� ����� ������� ��������� � �������� ����������� ���������� �����

(defun MT-level (/ A1 A2 A3 P0 P1 P2 P3 P4 F R N1 NN 
					dcl_id blockName userclick lev keyName)

	(setq sysvarsBak
		(list
			(cons "CMDECHO" (getvar "CMDECHO"))
			(cons "MENUECHO"(getvar "MENUECHO"))
			(cons "NOMUTT" (getvar "NOMUTT"))
			(cons "mirrtext" (getvar "mirrtext"))
			(cons "osmode" (getvar "osmode"))
		)
	); global
	
	(setvar "cmdecho" 0)
	(setvar "dimzin" 3)
	
	(vl-load-com)
	
	(setq aVer (atof (substr (getvar "ACADVER") 1 4)))
	(if (< aVer 16.2)
		(progn
			(alert "���������� ������ AutoCAD\n�� ���� 2006")
			(*error* "\n���������")
			(exit)
		)
	)
	
	(setq *A1 (list "50" "100" "200"))
	(setq F 0)
	(setq R 1)

	(setq dcl_id (load_dialog "MT_main.dcl"))
	(new_dialog "otm" dcl_id)

	(start_list "A1")
	(mapcar 'add_list *A1)
	(end_list)
	(defun sper (/)
		(if (= "1" (get_tile "FF")) 
			(setq F 1) (setq F 0)
		)
		(if (= "1" (get_tile "RR")) 
			(setq R 1) (setq R 0)
		)
	)

	(action_tile "A1" "(setq *A2 (get_tile \"A1\"))")
	(action_tile "accept" "(setq *A2 (get_tile \"A1\")) (sper) (SETQ userclick 1) (done_dialog)")
	(action_tile "cancel" "(SETQ userclick 0) (done_dialog)")

	(start_dialog)
	(unload_dialog dcl_id)

	(if (= *A2 "") ;�� ������
		(setq A2 100)
		(setq A2 (/ (atof (nth (atoi *A2) *A1))))
	);; A2- ��� �������� A3- ��� ����������� 

    (IF (= userclick 1)
        (PROGN
			(setq keyName "_commonLay")
			(MT-createLayer keyName)
			
			(if (= F 0)
				   (progn
						(setq A3 A2)
						(setq A2 1)
				   )
				   (setq A3 1)
			)
			(setvar "ORTHOMODE" 1)
			(setvar "osmode" 1015)
			(if (= R 1)
					(progn
						(setvar "attdia" 0)
						(setq P1 (getpoint "\n������� ������� ������� <0,000>: "))
					)
					(setvar "attdia" 1)
			)
			(if (= P1 nil)
				(setq P1 '(0 0 0))
			)
			(princ)
			(setq P2 (getpoint "\n \n������� ����� �� ������� <�����>: "))
			(princ)
			(if (= P2 nil)
				(setq N1 1) (setq N1 0)
			)
			
			(setvar "celtype" "ByLayer")
			(setvar "cecolor" "254")
			(setvar "CELWEIGHT" 18)
			
			(while (= N1 0)
				(princ)
				(setq P3 (getpoint P2 "\n \n������� ����� �������: "))
				(if (= P3 nil)
					(progn
						(*error* "\n������������ ����!")
						(exit)
					)
				)
				(princ)
				(setq P4 (getpoint P3 "\n \n������� ����������� <�����>: "))
				(if (= P4 nil)
					(setq P4 (list (car P3) (+ (cadr P3) 10)))
				)
				(setvar "osmode" 0)

				(setq P0 (list (car P3) (cadr P2)))
				(if (< (cadr P4) (cadr P2))
					  (Setq blockName "_level_down")
					  (Setq blockName "_level_up")
				)
				
				(if (not (tblsearch "block" blockName))
					(MT-createBlockLevel blockName)
				)
				
				(setvar "DIMLDRBLK" ".")
				(if (= R 1)
					(progn
						(setq lev (/ (* (- (cadr P2) (cadr P1)) A2) 1000))
						(command "_leader" P2 P0 "_Format" "_None" "" "" "_block" blockName P0 A3 A3 0 (rtos lev 2 3) "")
					)
					(command "_leader" P2 P0 "_Format" "_None" "" "" "_block" blockName P0 A3 A3 0)
				)
				(if (< (car P3) (car P2))
					(command "_mirror" (entlast) "" P0 (list (car P0) (+ (cadr P0) 1000)) "_y")
				)

				(setvar "osmode" 1015)
				(setq P2 (getpoint "\n \n������� ����� �� ������� <�����> :"))
				(princ)
				(if (= P2 nil)
					(setq N1 (1+ N1)) (setq N1 0)
				)
			)
			(setvar "cecolor" "256")
			(setvar "CELWEIGHT" -1)
			(setvar "ORTHOMODE" 0)
        )
    )
    (MT-restoreSysvars)
    (princ "\n���������")
	(princ)
)

(defun MT-createBlockLevel (blockName / NN height mult oldLayer)
; �������� ����� ������� 

	(setq height 2.5); ������ ������
	(setq oldLayer (getvar "clayer"))
	
	(vl-cmdf "_layer" "_set" "0" "")
	(setvar "celtype" "ByBlock")
	(setvar "cecolor" "7")
	(setvar "CELWEIGHT" 25)
	
	(if (= blockName "_level_up")
		(setq mult 1)
		(setq mult -1)
	)
	
	(setq  NN  (ssadd))
	(setvar "AFLAGS" 20)
	(vl-cmdf "_-attdef" "" "level" "�������" "%%p0.000" "_J" "_BR" (list (* height 6) (* (* height 2) mult) 0) height 0)
	(ssadd (entlast) NN)
	(vl-cmdf "_Pline" "0,0" "_W" "0" "0" (list 0 (* (* height 2) mult)) (list (* height 6) (* (* height 2) mult)) "")
	(ssadd (entlast) NN)
	(vl-cmdf "_Pline" (list (* height -1) (* height mult)) "_W" "0.5" "0.5" "0,0" (list height (* height mult)) "_W" "0" "0" "")
	(ssadd (entlast) NN)
	(vl-cmdf "_Line" (list (* height -2) 0 0) (list (* height 2) 0 0) "")
	(ssadd (entlast) NN)
	
	(vl-cmdf "_-BLOCK" blockName "0,0" NN "")
	
	(vl-cmdf "_layer" "_set" oldlayer "")
	
	(setvar "celtype" "ByLayer")
	(setvar "cecolor" "254")
	(setvar "CELWEIGHT" 18)
)

(load (findfile "MT_main.lsp"))
