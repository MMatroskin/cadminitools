;; Copyright (c) Maks Tulin 2011/2018
;;

(defun *error* (msg)
		
		(setvar "filedia" 1)
		(setvar "CMDECHO" 1)
		
		(terpri)
		(princ msg)
		
); end of *error*

(defun MT-save-path (/ pref_obj path f-name file aa)
  
	(setq f-name "acadpath.dat")
	(setq pref_obj (vla-get-Files (vla-get-preferences (vlax-get-Acad-Object))))
	(setq path (list (vla-get-SupportPath pref_obj)))
	(setq path (append path (list (vla-get-QNewTemplateFile pref_obj))))
	(setq path (append path (list (vla-get-TemplateDwgPath pref_obj))))
	(setq path (append path (list (vla-get-PrinterStyleSheetPath pref_obj)))); ����� ������
	(setq path (append path (list (vla-get-PrinterConfigPath pref_obj))));��������
	(setq path (append path (list (vla-get-PrinterDescPath pref_obj))));�������� ��������
	(if (not (findfile f-name))
		(progn
			(setq 
				file (strcat (getvar "ROAMABLEROOTPREFIX") "Support\\" f-name) 
				handle (open file "w")
			)
			(close handle)
		)
	)

	(MT-writeListToFile f-name path T)	

)

(defun MT-startup (path / item file file1 file0 handle result)
; �������� ����� ��� ����������� (���� ���� ���� - ����������)

	(vl-load-com)
	(setq 
		file (strcat (getvar "ROAMABLEROOTPREFIX") "Support\\acad.lsp") 
		handle (open file "w")
		result T
	)
	(foreach item
		(list 
			"(defun S::STARTUP ()" 
			"    (load (findfile \042MT_loader.lsp\042))" 
			"    (MT-loader)" 
			")"
		)
		(write-line item handle)
	)
	(close handle)
	
	(setq file1 (strcat (getvar "ROAMABLEROOTPREFIX") "Support\\MT_loader.lsp"))
	
	(if (findfile file1)
		(vl-file-delete file1)
	)
	(if(setq file0 (findfile (strcat path "\\Main\\MT_starter.lsp")))
		(vl-file-copy 
			file0
			file1
		); ��������� ����������� ���� ������ ����� ����� ��������� � �������� �������
		(setq result nil)
	)
	result

);defun

(defun MT-setup (path dir noSilent / Aver menu_ver file part str tmp tmpStr exProfiles acadprefs acadprofiles file0 file1 files
								handle sep localeId logFile p_name0 p_name1 prof_list d_support new_support msg autoLoad
								file new_plotstyle new_templ msg path0 path1 pref_obj x oldTrustedPaths msgUuser strCommon)
; call: (MT-setup "C:\\CMT" T)

	(setvar "cmdecho" 0)
	(setvar "filedia" 0)
	
	(vl-load-com)
	(setq Aver (atof (substr (getvar "ACADVER") 1 4)))
;	(if(> Aver 19.0)
;			(setvar "SECURELOAD" 0)
;	)
	(setq path1 (strcat path dir))
	(load (findfile (strcat path1 "\\Main\\MT_main.lsp")))
	
  	;(setq Aver (atof (MT-getNumberAcadVer)))
	(setq localeId (MT-getLocaleId))
	(setq strCommon (MT-getAcadName))
	(cond
		((= localeId "419")
			(setq p_name0 "<<������� ��� �����>>")
		); rus
		((= localeId "409")
			(setq p_name0 "<<Unnamed Profile>>")
		); eng
		(t 
			(setq p_name0 nil)
		)
	)
	(setq logFile
		(strcat path1 "\\Main\\MiniTools.log")
	)
	(if (not (findfile logFile))
		(progn
			(setq 
				file logFile 
				handle (open file "w");
			)
			(close handle)
		)
	); ������� ������ ���� �� ������
	
	(if (= p_name0 nil)
		(progn
			(setq msg 
				(strcat 
					(MT-getCurrentDate) 
					" - "
					strCommon
					" - "
					"Integration error. Unknown locale. "
				)
			)
			(MT-finishSetup logFile msg)
			(exit)
		)
	)
	
	(setq menu_ver 
		(cond 
			((>= Aver 18.0) ".cuix")
			((>= Aver 16.2) ".cui")
			(t ".mnu")
		)
	)
	
	(if (>= Aver 18.0)
		(progn
			(setq acadprefs (vla-get-preferences (vlax-get-Acad-Object)))
			(setq acadprofiles (vla-get-profiles acadprefs))

			(setq prof_list (vla-GetAllProfileNames acadprofiles 'res))
			(setq prof_list (vlax-safearray->list res))
			
			(if (null (VL-POSITION p_name0 prof_list))
				(progn
					(setq msg 
						(strcat 
							(MT-getCurrentDate) 
							" - "
							strCommon
							" - "
							"Integration error. Standard profile not found. "
						)
					)
					(MT-finishSetup logFile msg)
					(exit)
				)
			)

			(vla-put-ActiveProfile acadProfiles p_name0)
			(MT-save-path)
			
			(setq file 
				(strcat path1 "\\Main\\" "MiniTools.cfg")
			)
			
			(setq sect "exepts")
			(setq str 
				(MT-getValue file sect "_profiles")
			) ; ������ ���������� (�������, ���� �� �������)
			
			(setq tmpStr (getenv "USERPROFILE"))
			(setq path0 (vl-string-subst "" tmpStr (getvar "ROAMABLEROOTPREFIX")))
			(setq path0 (strcase path0 T))
			(setq path0 (strcat tmpStr path0)) ; ���� � ACAD ����������������
			(setq path0 (strcat path0 "support"))
			
			(setq part "main")
;;;			(setq path1 (MT-getValue file part "_path")) ; basePath	
			(cond 
;;;				((= Aver 19.0)
;;;					(setvar "SECURELOAD" 1)
;;;				)
				((> Aver 19.0)
;;;					(setvar "SECURELOAD" 1)
					(setq oldTrustedPaths (getvar "trustedpaths"))
					(if (= (vl-string-search path0 oldTrustedPaths) nil)
						(setvar "trustedpaths" 
							(strcat oldTrustedPaths ";" path0)
						)
					)
					(princ)
					(princ (getvar "trustedpaths"))
				)
				(t (princ "\nOk"))
			) ; or to write acad.lsp in "%AppData%\Autodesk\ApplicationPlugins\..." ???

			(setq exProfiles nil)
			(if str
				(progn
					(setq sep ",")
					(while (setq pos (vl-string-search sep str))
						(setq tmp (substr str 1 pos))
						(setq str (substr str (+ (strlen sep) 1 pos)))
						(setq exProfiles (append exProfiles (list tmp)))
					)
					(setq exProfiles (append exProfiles (list str)))
				)
			)
			(princ)
			
			(if exProfiles
				(foreach x exProfiles
					(setq prof_list (vl-remove x prof_list))
				)
			); ������ �������� ��� ��������� � ������ ����������
			(princ)
		  
			(setq p_name1 "CAD-MT")
			(if (VL-POSITION p_name1 prof_list)
				(progn 
					(setq prof_list (VL-REMOVE p_name1 prof_list))
					(vlax-invoke-method acadProfiles 'DeleteProfile p_name1)
				)
			)
			(vlax-invoke-method acadProfiles 'CopyProfile p_name0 p_name1)
			(setq prof_list (append prof_list (list p_name1)))
			(setq prof_list (VL-REMOVE p_name0 prof_list))
			(princ)

			(setq d_support 
				(strcat 
					";" 
					path1 
					"\\Main;" 
					path1 
					"\\Plugins"
				)
			)
			
			(setq pref_obj (vla-get-Files acadprefs))
			(setq new_plotstyle 
				(strcat 
					(vla-get-PrinterStyleSheetPath pref_obj)
					";"
					path1
					"\\Support"
				)
			)
		  	(princ)
			(princ new_plotstyle)
		  
			(setq new_templ 
				(strcat (getenv "TemplatePath") "\\acadiso.dwt")
			)
			
			(vla-put-SaveAsType (vla-get-opensave acadprefs) 48) ; saveas 2010
			(setvar "ACADLSPASDOC" 1); autoload
			(setvar "DEMANDLOAD" 2)
			(setvar "PROXYNOTICE" 1)
			(setvar "PROXYSHOW" 1)
			(setvar "PICKFIRST" 1)
;;;			(setvar "LAYOUTTAB" 1)
			
			(mapcar 
				'(lambda (x)
					(progn
						(expand 2)
						(princ)
						(vla-put-ActiveProfile acadProfiles x)
						(setq pref_obj
							(vla-get-Files
								(vla-get-preferences
									(vlax-get-Acad-Object)
								)
							)
						)
					  	(princ x)
						(setq new_support 
							(strcat (vla-get-SupportPath pref_obj) d_support)
						)
					  	(princ new_support)
					  	(vla-put-SupportPath pref_obj new_support)
						
						; (vla-put-PrinterStyleSheetPath pref_obj new_plotstyle); wtf?
						; ��� ������ ������ autocad �� ��������
						; http://adn-cis.org/prostaya-alternativa-ispolzovaniyu-com-obekta-preferences-v-autocad.html
						
						(setenv "PrinterStyleSheetDir" new_plotstyle)
						
						(if (and
								(= (getvar "cprofile") p_name1)
								(findfile new_templ)
							)
							(vla-put-QNewTemplateFile pref_obj new_templ) ; 
						)
						
						(vla-put-SaveAsType (vla-get-opensave acadprefs) 48) ; saveas 2010

						(if (>= Aver 17.2)
							(progn 
								(setvar "MENUBAR" 1)
								(setvar "WSAUTOSAVE" 0)
							)
						)
						
						(cond 
							((= Aver 19.0)
								(princ Aver)
								;(setvar "AUTOLOAD" 1)
							)
							((> Aver 19.0)
								(setvar "SECURELOAD" 1)
								(setq oldTrustedPaths (getvar "trustedpaths"))
								(princ oldTrustedPaths)
								(cond 
									((and
										(vl-string-search path0 oldTrustedPaths)
										(= (vl-string-search path oldTrustedPaths) nil))
										(setvar "trustedpaths" 
											(strcat oldTrustedPaths ";" path "\\...")
										)
									)
									((and
										(vl-string-search path oldTrustedPaths)
										(= (vl-string-search path0 oldTrustedPaths) nil))
										(setvar "trustedpaths" 
											(strcat oldTrustedPaths ";" path0)
										)
									)
									((and
										(vl-string-search path oldTrustedPaths)
										(vl-string-search path0 oldTrustedPaths))
										(princ "Ok")
									)
									(t 
										(setvar "trustedpaths" 
											(strcat path0 ";" path "\\...")
										)
									)
								)
								(princ)
								(princ (getvar "trustedpaths"))
							)
							(t (princ "\nOk"))
						)
						
						(setvar "fullplotpath" 0)
						;(setvar "fontalt" "simplex.shx")
						(setvar "DEMANDLOAD" 2)
						(setvar "ACADLSPASDOC" 1)
						(setvar "attdia" 1)
						(setvar "PROXYNOTICE" 1)
						(setvar "PROXYSHOW" 1)
						(setvar "xloadctl" 2)
						(setvar "XREFTYPE" 1)
						(setvar "PICKFIRST" 1)
;;;						(setvar "LAYOUTTAB" 1)
						(setvar "LWDEFAULT" 18)
					)
				)
				prof_list
			)

			(vla-put-ActiveProfile acadProfiles p_name0)
			(setvar "ACADLSPASDOC" 0) ; for default profile - do not load MT-loader when opening each drawing (at the start ACAD only)
			
			; change file codepage http://forum.dwg.ru/showthread.php?t=81246
			(setq files (list "MT_main" "MT_stamp"))
			(foreach x files
				(setq file1 (strcat path1 "\\Main\\" x ".dcl"))
				(if (findfile file1)
					(vl-file-delete file1)
				)
				(if (= Aver 19.0)
					(progn
						(if(setq file0 (findfile (strcat path1 "\\Main\\" x "-UTF8.dcl")))
							(vl-file-copy 
								file0
								file1
							)
						)
					)
					(progn
						(if(setq file0 (findfile (strcat path1 "\\Main\\" x "-ANSI.dcl")))
							(vl-file-copy 
								file0
								file1
							)
						)
					)
				)
			)
			
			
			(setq autoLoad (MT-startup path1))
			(setq msgUser "���������.\n������������� AutoCAD")
			(if (= autoLoad nil)
				(setq msg 
					(strcat 
						(MT-getCurrentDate) 
						" - "
						strCommon
						" - "
						"Error! Load script not created! "
					)
				)
				(setq msg 
					(strcat 
						(MT-getCurrentDate) 
						" - "
						strCommon
						" - "
						"Integration successfuly completed. "
					)
				)
			)
			
		)
		(progn
			(setq msgUuser "������!\n��� ������ AutoCAD\n�� ��������������")
			(setq msg 
				(strcat 
					(MT-getCurrentDate) 
					" - "
					strCommon
					" - "
					"Integration Error. ACAD version is not supported. "
				)
			)
		)
		
	)
	
	(if noSilent
		(MT-showMsg msgUser)
	)

	(MT-finishSetup logFile msg)
	
	(setvar "filedia" 1)
	(setvar "cmdecho" 1)
	
)

(defun MT-remove (path dir noSilent / d_support tmp path00 path1 pathTmp support_path new_templ plot_style exProfiles 
								str p_name0 p_name1 prof_list acadProfiles pref_obj fileList file part 
								oldTrustedPaths newTrustedPaths remStr Aver sep localeId logFile handle strCommon)

	(setvar "cmdecho" 0)
	(setvar "filedia" 0)
		
	(vl-load-com)
	(setq path1 (strcat path dir))
	(load (findfile (strcat path1 "\\Main\\MT_main.lsp")))
	
  	(setq Aver (atof (MT-getNumberAcadVer)))
	(setq localeId (MT-getLocaleId))
	(setq strCommon (MT-getAcadName))
	(cond
		((= localeId "419")
			(setq p_name0 "<<������� ��� �����>>")
		)
		((= localeId "409")
			(setq p_name0 "<<Unnamed Profile>>")
		)
		(t 
			(setq p_name0 nil)
		)
	)

	(setq logFile
		(strcat path1 "\\Main\\MiniTools.log")
	)
	(if (not (findfile logFile))
		(progn
			(setq 
				file logFile 
				handle (open file "w");
			)
			(close handle)
		)
	); ������� ������ ���� �� ������
	
	(setq tmp (substr path1 1 1))
	(setq path00 (vl-string-left-trim tmp path1))
	(setq path00 (strcase path00 T)) ; ACAD ������������ � �������� � �������
	(setq path00 (strcat tmp path00))
  
	(setq file 
		(strcat path1 "\\Main\\" "MiniTools.cfg")
	)
	(setq part "main")
;;	(setq path1 (MT-getValue file part "_path")) ; basePath
	(setq part "exepts")
	(setq str 
		(MT-getValue file part "_profiles")
	) ; ������ ����������
	(setq exProfiles nil)
	(if str
		(progn
			(setq sep ",")
			(while (setq pos (vl-string-search sep str))
				(setq tmp (substr str 1 pos))
				(setq str (substr str (+ (strlen sep) 1 pos)))
				(setq exProfiles (append exProfiles (list tmp)))
			)
			(setq exProfiles (append exProfiles (list str)))
		)
	)
	
	(setq acadprofiles (vla-get-profiles (vla-get-preferences (vlax-get-Acad-Object))))
	(setq prof_list (vla-GetAllProfileNames acadprofiles 'res))
	(setq prof_list (vlax-safearray->list res))
	
	(if (null (VL-POSITION p_name0 prof_list))
		(progn
			(setq msg 
				(strcat 
					(MT-getCurrentDate) 
					" - "
					strCommon
					" - "
					"Error  of removing. Standard profile not found. "
				)
			)
			(MT-finishSetup logFile msg)
			(exit)
		)
	)

	(setq prof_list (VL-REMOVE p_name0 prof_list))
	(if exProfiles
		(foreach x exProfiles
			(setq prof_list (vl-remove x prof_list))
		)
	); ������ �������� ��� ��������� � ������ ����������
  
	(setq p_name1 "CAD-MT")

	(vla-put-ActiveProfile acadProfiles p_name0)
	(if (VL-POSITION p_name1 prof_list)
		(progn 
			(setq prof_list (VL-REMOVE p_name1 prof_list))
			(vlax-invoke-method acadProfiles 'DeleteProfile p_name1)
		)
	)

	(setq pathTmp (vl-string-subst "" "\\CADMiniTools" path))
 	(princ pathTmp)
	(setq d_support 
		(strcat 
			";" 
			pathTmp 
			"\\cadminitools\\minitools\\main;" 
			pathTmp 
			"\\cadminitools\\minitools\\plugins"
		)
	)
	(princ d_support)
	
	(mapcar 
		'(lambda (x)
			(progn
				(vla-put-ActiveProfile acadProfiles x)

				(setq pref_obj (vla-get-Files (vla-get-preferences (vlax-get-Acad-Object))))
				(setq support_path (vla-get-SupportPath pref_obj))
				(setq plot_style (vla-get-PrinterStyleSheetPath pref_obj))

				(setq support_path (vl-string-subst "" d_support support_path))
				
				(setq plot_style 
					(vl-string-subst 
						"" 
						(strcat 
							";"	
							;(strcase path00 T) ; �� ������, �� � ���� ����� ���
							pathTmp ; ��� ���
							"\\cadminitools\\minitools\\support"
						) 
						plot_style
					)
				)

				;(setvar "fontalt" "Simplex.shx")
				(vla-put-SupportPath pref_obj support_path)
				(vla-put-PrinterStyleSheetPath pref_obj plot_style)
				
				(setq remStr (strcat path  "\\..."))
				(princ remStr)
				(if (> Aver 19.0)
					(progn
						(setq oldTrustedPaths (getvar "trustedpaths"))
						(if (setq pos (vl-string-search remStr oldTrustedPaths))
							(progn
								(if (=  pos 0)
									(if (/= oldTrustedPaths remStr)
										(setq remStr (strcat remStr ";"))
									)
									(setq remStr (strcat ";" remStr))
								)
								(setq newTrustedPaths
									(vl-string-subst "" remStr oldTrustedPaths)
								)
								(setvar "trustedpaths" newTrustedPaths)
							 	(princ)
								(princ (getvar "trustedpaths"))
							)
						)
					)
				)
				;(setvar "ACADLSPASDOC" 0) ; - ����� ���� ������ ����������
			)
		) prof_list
	);mapcar
	
	(vla-put-ActiveProfile acadProfiles p_name0)

	(setq fileList 
		(list 
			(strcat (getvar "ROAMABLEROOTPREFIX") "Support\\acad.lsp")
			(strcat (getvar "ROAMABLEROOTPREFIX") "Support\\MT_loader.lsp")
		)
	)
	(foreach x fileList
		(if (findfile x)
			(vl-file-delete x)
		)
	)
	
	(setq msg "���������.\n������������� AutoCAD")
	(if noSilent
		(MT-showMsg msg)
	)
	
	(setq msg 
		(strcat 
			(MT-getCurrentDate) 
			" - "
			strCommon
			" - "
			"Removing successfuly completed. "
		)
	)
	(MT-finishSetup logFile msg)
	
	(setvar "filedia" 1)
	(setvar "cmdecho" 1)
	
);defun

(defun MT-finishSetup (logFile msg / lst)

	(setq lst (list msg))
	(MT-writeListToFile logFile lst nil)
	(princ)
)

(defun MT-showMsg (msg / )
	(alert msg)
); ��� ����� ��������� �� ��������
