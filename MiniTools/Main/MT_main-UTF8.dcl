﻿od :dialog {label="Лист общих данных";
      :row {
      :boxed_column
              {label="Таблицы";
               fixed_width=true;
               width=15;
       :toggle   {label= "Ведомость чертежей";
                  key="AA";
                  fixed_width = true;
                 }
       :toggle   {label= "Ведомость комплектов";
                  key="BB";
                  fixed_width = true;
                 }
       :toggle   {label= "Ведомость документов";
                  key="SS";
                  fixed_width = true;
                 }
              }
      :boxed_column
              {
               fixed_width=true;
               width=15;
       :toggle   {label= "Ведомость разработок";
                  key="FF";
                  fixed_width = true;
                 }
       :toggle   {label= "Штамп ГИП";
                  key="HH";
                  fixed_width = true;
                 }
       :text {key="odtext";
                 value="Выберите \nнеобходимые \nэлементы.";
                 width=15;
                 height=5;
                 }
              } 
           }
ok_cancel;
}

tbl_blk :dialog {label="Вид таблицы";
	: boxed_radio_column {
		fixed_width=true;
		width=15;
		:radio_button {
			key = AA;
			label = "Таблица";
			value = "1";
		}
		:radio_button {
			key = BB;
			label = "Блок";
		}
	}
ok_cancel;
}

otm :dialog {label="Отметка";
      :row {
      :boxed_column{
               fixed_width=true;
               width=15;
      :popup_list {label= "Mасштаб, 1:";
                 key="A1";
                 edit_width=7;
                 height=5;
                 value = "1";
                  }
               }
      :boxed_column{
               fixed_width=true;
               width=15;
       :toggle   {label= "От баз. уровня";
                  key="RR";
                  fixed_width = true;
                  value = "1";
                 }
       :toggle   {label= "На листе";
                  key="FF";
                  fixed_width = true;
                 }
               }
           }
ok_cancel;
}

otv :dialog {label="Отверстие прямоугольное";
      :row {
      :boxed_column
              {label="Параметры";
               fixed_width=true;
               width=20;
       :edit_box {label="Длинна, мм (X)";
                  key="A1";
                  edit_width=8;
                 }
       :edit_box {label="Ширина, мм (Y)";
                  key="B1";
                  edit_width=8;
                 }
              }
           }
ok_cancel;
}

poz :dialog {label="Выноски";
      :row {
      :boxed_column{
       :edit_box {label="Позиция";
                  key="A1";
                  edit_width=6;
                 }
       :edit_box {label="Количество";
                  key="B1";
                  edit_width=6;
                 }
             }
          }
ok_cancel;
}

Welding :dialog {label = "Выберите тип шва";
           : row{
              : boxed_radio_column {
                fixed_width=true;
                   :radio_button {
                    key = AA1;
                    label = "заводской";
                    value = "1";
                    }
                  fixed_width=true;
                  width=15;
                    :radio_button {
                     key = AA2;
                     label = "Монтажный";
                    }
               }
              : boxed_radio_column {
                fixed_width=true;
                   :radio_button {
                    key = BB1;
                    label = "Стыковой";
                    value = "1";
                    }
                  fixed_width=true;
                  width=15;
                    :radio_button {
                     key = BB2;
                     label = "Угловой";
                    }
               }
              : boxed_radio_column {
                fixed_width=true;
                   :radio_button {
                    key = CC1;
                    label = "Сплошной";
                    value = "1";
                    }
                  fixed_width=true;
                  width=15;
                    :radio_button {
                     key = CC2;
                     label = "Прерывистый";
                    }
               }
           }
           :boxed_row {
ok_cancel;
           }
}

dwg_frame :dialog {label = "Рамка чертежа";
	:row {
		:popup_list {label = "Формат: ";
			key = "AA1";
			edit_width = 10;
			height = 5;
			value = "";
		}
	}
	:row {
		ok_cancel;
	}
}

axis :dialog {label = "Одиночная ось";
      :row {
      :boxed_column
              {label= "Отступ";
               fixed_width=true;
               width=15;
       :edit_box {label= "От первой точки";
                  key= "BB1";
                  edit_width=5;
                 }
       :edit_box {label= "От второй точки";
                  key= "BB2";
                  edit_width=5;
                 }
              }
      :boxed_column
              {
               fixed_width=true;
               width=15;
      :popup_list {label= "Mасштаб, 1:";
                 key="A1";
                 edit_width=5;
                 height=5;
                 value = 3;
                  }
       :toggle   {label= "Подписать";
                  key="FF";
                  fixed_width = true;
                 }
              }
           }
      :row {
      :text    {key="osstext";
                 value="При оформлении на листе выбрать масштаб 1:1.";
               }
           }

ok_cancel;
}

m_axis :dialog {label = "Сетка осей";
      :row {
      :boxed_column
              {label = "Шаг";
               fixed_width=true;
               width=8;
       :edit_box {label="По горизонтали";
                  key="SS1";
                  edit_width=6;
                 }
       :edit_box {label="По вертикали";
                  key="SS2";
                  edit_width=6;
                 }
              }
      :boxed_column
              {label = "Пролеты";
               fixed_width=true;
               width=8;
       :edit_box {label= "По горизонтали";
                  key="RR1";
                  edit_width=6;
                 }
       :edit_box {label= "По вертикали";
                  key="RR2";
                  edit_width=6;
                 }
              }
           }
       
      :boxed_row {
      :popup_list {label= "Mасштаб отображения на листе, 1:";
                 key="A1";
                 edit_width=8;
                 value = 3;
                  }
           }
ok_cancel;
}

axis_mark :dialog {label = "Маркер оси";
      :row {
      :boxed_column
            {
      :row   {
      :popup_list {label = "Mасштаб отображения, 1:";
                 key="A1";
                 edit_width=5;
                 height=5;
                 value = "";
                  }
            }
      :row  {
      :text {key="ospodtext";
                 value="При оформлении на листе \nвыбрать масштаб 1:1.";
                 width=15;
                 height=2;}
            }
           }
          }
ok_cancel;
}


axis_gp :dialog {label = "Оси на ГП";
      :row {
      :boxed_column
            {
      :row   {
      :popup_list {label = "Mасштаб отображения, 1:";
                 key="A1";
                 edit_width=5;
                 height=5;
                 value = 2;
                  }
            }
      :row  {
      :text {key="ospodtext";
                 value="При оформлении на листе \nвыбрать масштаб 1:1.";
                 width=15;
                 height=2;}
            }
           }
          }
ok_cancel;
}


coord :dialog {label="Координаты";
	:row   {
		:popup_list {
			label= "Mасштаб, 1:";
			key="AA";
                 	edit_width=8;
                 	height=5;
                 	value = 1;
		}
	}
	:row  {
		:toggle   {
			label= "На листе";
                  	key="FF";
                  	fixed_width = true;
		}
	}
ok_cancel;
}

CHMENU :dialog {label = "Выбор группы меню";
	:row  {
		:boxed_column {
			fixed_width=true;
			width=15;
			:popup_list {
				key = "AA1";
				height=5;
				value = "";
			}
			:toggle   {
			label= "Запомнить";
                  	key="FF";
                  	fixed_width = true;
			}
		}
		:boxed_column {
			fixed_width=true;
			width=15;
			:text {
				value = "Выберите <<MT-общее>>\nчтобы выгрузить\nдоп. меню";
				height=5;
			}
		}
	}
ok_cancel;
}

common_mark :dialog {label = "Подписать";
      :row {
      :boxed_column
            {
      :row   {
      :popup_list {label = "Mасштаб отображения, 1:";
                 key="A1";
                 edit_width=5;
                 height=5;
                 value = 3;
                  }
            }
      :row  {
      :text {key="ospodtext";
                 value="При оформлении на листе \nвыбрать масштаб 1:1.";
                 width=15;
                 height=2;}
            }
           }
          }
ok_cancel;
}

select_mark :dialog {label = "Выбор";
      :row {
      :boxed_column
            {
      :popup_list {label = "Опция:";
                 key="A1";
                 edit_width=15;
                 height=15;
                 value = 1;
                  }
            }
          }
ok_cancel;
}

selection_list :dialog {label = "Выбор элемента";
      :row {
      :boxed_column
              {
               fixed_width=true;
               width=40;
      :list_box  {label = "Доступно:";
                 key="A1";
                 edit_width=35;
                 height=15;
                 }
              }
           }
ok_cancel;
}