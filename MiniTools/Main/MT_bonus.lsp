;; Copyright (c) Maks Tulin 2018
;;
;; ������ �������� ������ �������
;; ��������� ��������� � �������� .\\_bonus

(defun runExplodeproxy ( / acadVer appVer acadBitVer appName dir fullAppName)
; ������ ���������, ������� ���������� ��� proxy-��������� �� ��������� �������
	
	(setvar "CMDECHO" 0)
	
	(vl-load-com)

	(setq acadVer (MT-getAcadVer))
	(setq appVer
		(cond
			(
				(= (atoi acadVer) 2019)
				2019
			)
			(
				(= (atoi acadVer) 2018)
				2018
			)
			(
				(= (atoi acadVer) 2017)
				2017
			)
			(
				(or 
					(= (atoi acadVer) 2016)
					(= (atoi acadVer) 2015)
				)
				2015
			)
			(
				(and 
					(< (atoi acadVer) 2015)
					(> (atoi acadVer) 2012)
				)
				2013
			)
			(
				(and 
					(< (atoi acadVer) 2013)
					(> (atoi acadVer) 2009)
				)
				2010
			)
			(
				(= (atoi acadVer) 2009)
				2009
			)
			(
				(or 
					(= (atoi acadVer) 2008)
					(= (atoi acadVer) 2007)
				)
				2007
			)
			(
				(and 
					(< (atoi acadVer) 2007)
					(> (atoi acadVer) 2002)
				)
				2006
			)
			(
				(and 
					(< (atoi acadVer) 2004)
					(>= (atoi acadVer) 2000)
				)
				2000
			)
			(t 0)
		)
	)
	(if (> appVer 2000)
		(progn
			(if (<= appVer 2007)
				(setq acadBitVer "")
				(setq acadBitVer (MT-whatAcadBitVer))
			)
			(if (= acadBitVer "x86")
				(setq acadBitVer "x32")
			)
			(setq appName "ExplodeProxy")
			(setq dir ".\\_bonus\\")
			(if 
				(not
					(setq fullAppName
						(findfile
							(strcat 
								dir 
								"ExplodeProxy\\" 
								appName 
								(itoa appVer)
								acadBitVer
								".arx"
							)
						)
					)
				)
				(princ "\n���������� �� �������. \n�����.. ")
				(progn
					(arxload fullAppName)
					(vl-cmdf "EXPLODEALLPROXY")
					(vl-cmdf "REMOVEALLPROXY" "_y")
					(vl-cmdf "_audit" "_y")
				)
			)
		)
	)
	
	(setvar "CMDECHO" 1)

)

(defun loadBonusLispFile (appName / dir fullAppName result)
; ������ Lisp - ��������

	(setq dir ".\\_bonus\\")
	(if 
		(setq fullAppName
			(findfile
				(strcat 
					dir 
					appName
				)
			)
		)
		(progn
			(load fullAppName)
			(setq result T)
		)
		(setq result nil)
	)

)

(load (findfile "MT_main.lsp"))
