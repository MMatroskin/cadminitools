stamp_0 : dialog{
	width=72;
	fixed_width = true;
	key = "Title";
	label = "���������� �������� �������";
	:row {
		:column{
			:boxed_column{
				:edit_box {
					label = "����";
					key = "Title_0";
					edit_width=64;
					height = 1.1;
					fixed_height = true;
				}
				:edit_box {
					label = "������";
					key = "Subject_0";
					edit_limit = 256;
					edit_width=64;
					height = 1.1;
					fixed_height = true;
				}
				:edit_box {
					label = "������";
					key = "Comments_0";
					edit_limit = 256;
					edit_width=64;
					height = 1.1;
					fixed_height = true;
				}
				:edit_box {
					label = "������";
					key = "Keywords_0";
					edit_width=64;
					height = 1.1;
					fixed_height = true;
				}
				:edit_box {
					label = "���� �������";
					key = "_release_date_0";
					edit_width=64;
					height = 1.1;
					fixed_height = true;
				}
				spacer;
			}
			:boxed_column{
				:edit_box {
					label = "����������";
					key = "_author1_0";
					edit_width=64;
					height = 1.1;
					fixed_height = true;
				}
				:edit_box {
					label = "����������";
					key = "_author2_0";
					edit_width=64;
					height = 1.1;
					fixed_height = true;
				}
				:edit_box {
					label = "��������";
					key = "_inspector_0";
					edit_width=64;
					height = 1.1;
					fixed_height = true;
				}
				:edit_box {
					label = "��������� ������";
					key = "_head_of_dept_0";
					edit_width=64;
					height = 1.1;
					fixed_height = true;
				}
				:edit_box {
					label = "H���. ��������";
					key = "_standard_control_0";
					edit_width=64;
					height = 1.1;
					fixed_height = true;
				}
				:edit_box {
					label = "���";
					key = "_project_manager_0";
					edit_width=64;
					height = 1.1;
					fixed_height = true;
				}
				spacer;
			}
			:boxed_column{
				:edit_box {
					label = "�����������";
					key = "_company_0";
					edit_width=64;
					height = 1.1;
					fixed_height = true;
				}
				spacer;
			}
		}
	}
	:row{
		:toggle{
			key = "togSepStr";
			value = "0";
			fixed_height = true;
			alignment = left;
			label = "��������� ��������� �� ������ ( ������ ' \\n' )";
		} 
	}
	:row{
		:spacer{
			width=48;
		}
		ok_cancel;
	}
}
