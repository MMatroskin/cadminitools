;; Copyright (c) Maks Tulin 2018
;;
;; startup functions
;;

(defun *error* (msg)

		(setvar "filedia" 1)
		(setvar "MENUECHO" 0)
		(setvar "NOMUTT" 0)
		(setvar "CMDECHO" 1)
		(terpri)
		(princ msg)
  
); end of *error*

(defun MT-loader ( / oldEcho aVer acadProfiles p_name1 p_name0 prof_list pos filesList 
			fullFileName msg tmpValueList lTypesListTmp lTypesList layersList lTypeFile 
			sectionName curLayer txtStylePropertyList txtStylesList dimStylesList menuList 
			configFileName sep tmp x cmdList res dimensionLayer layersConfig layersConfigList 
			layerPropertyList layersDefaultList layersConfigTmpList value localeId)
				
; load MT utilities, load menu, setup sysvars and create layers/styles
				
	(setq oldEcho (getvar "cmdecho"))
	(setvar "cmdecho" 0)
	(setq curLayer (getvar "clayer"))
	(setq p_name1 "CAD-MT")
	
	(vl-load-com)
	
	(setq aVer (substr (getvar "ACADVER") 1 4))
	(setq value 
		(vl-registry-read 
			(strcat "HKEY_CURRENT_USER\\Software\\Autodesk\\AutoCAD\\R" aVer)
			"CurVer"
		)
	)
	(setq pos (vl-string-position (ascii ":") value))
	(setq localeId (substr value (+ pos 2)))
	(cond
		((= localeId "419")
			(setq p_name0 "<<������� ��� �����>>")
		); rus
		((= localeId "409")
			(setq p_name0 "<<Unnamed Profile>>")
		); eng
		(t 
			(progn
				(setq msg (strcat "\n������ ��������! ������� �� ��������� �� ������!\n��������"))
				(*error* msg)
				(exit)
			)
		)
	)

	(setq acadProfiles 
		(vla-get-profiles 
			(vla-get-preferences 
				(vlax-get-Acad-Object)
			)
		)
	)
	(setq prof_list 
		(vla-GetAllProfileNames acadProfiles 'res)
	)
	(setq prof_list 
		(vlax-safearray->list res)
	)
	
	(if (VL-POSITION p_name1 prof_list)
		(if (= (getvar "cprofile") p_name0)
			(vla-put-ActiveProfile acadProfiles p_name1)
		)
		(progn
			(setq msg (strcat "\n������ ��������! ������� CAD-MT �� ������!\n��������"))
			(terpri)
			(princ msg)
			(exit)
		)
	); set profile

	(if (not 
			(setq configFileName 
				(findfile 
					"MiniTools.cfg"
				)
			)
		)
		(progn
			(setq msg (strcat "\n���� ������������ �� ������!\n������� CAD-MT �� ���������."))
			(setvar "filedia" 1)
			(setvar "MENUECHO" 0)
			(setvar "NOMUTT" 0)
			(setvar "CMDECHO" 1)
			(terpri)
			(princ msg)
			(exit)
		); exit NO ERROR
	)
	
;	(setvar "fullplotpath" 0); - ��� ���������
;	(setvar "fontalt" "Simplex.shx"); - ��� ���������
;	(setvar "DEMANDLOAD" 2); - ��� ���������
;	(setvar "ACADLSPASDOC" 1); - ��� ���������
;	(setvar "startup" 0); ����� ������ ������������
;	(setvar "LAYOUTTAB" 1); - ������ ������������
	
	(setvar "xloadctl" 2)
	(setvar "MIRRTEXT" 0)
	(setvar "PROXYSHOW" 1)
	(setvar "PROXYNOTICE" 1)
	(setvar "XREFTYPE" 1)
	(setvar "LWDEFAULT" 18)
	(setvar "INSUNITS" 4)
	(setvar "INSUNITSDEFSOURCE" 4)
	(setvar "INSUNITSDEFTARGET" 4)
	(setvar "LUNITS" 2)
	(setvar "LUPREC" 2)
	
	(setvar "CELTYPE" "ByLayer")
	(setvar "CECOLOR" "256")
	(setvar "CELWEIGHT" -1); setup sysvars
	
	(setq filesList
		(list
			"MT_main.lsp"
			"MT_styles.lsp"
			"MT_menu.lsp"
		)
	)
	(foreach x filesList
		(progn
			(if (= (setq fullFileName (findfile x)) nil)
				(progn
					(setq msg (strcat "\n������ ��������! ���� " x " �� ������!\n��������"))
					(*error* msg)
					(exit)
				)
				(load fullFilename)
			)
		)
	); load LSP files
	
	(setq sectionName "linetypes_default")
	(setq tmpValueList (MT-getConfigList configFileName sectionName))
	(setq lTypeFile (cdr (assoc "_lTypeFile" tmpValueList)))
	(setq lTypesListTmp (cdr (assoc "_lTypes" tmpValueList)))
	(setq sep ",")
	(while (setq pos (vl-string-search sep lTypesListTmp))
		(setq tmp (substr lTypesListTmp 1 pos))
		(setq lTypesListTmp (substr lTypesListTmp (+ (strlen sep) 1 pos)))
		(setq lTypesList (append lTypesList (list tmp)))
	)
	(setq lTypesList (append lTypesList (list lTypesListTmp))) ; load default linetypes
	
	; (setq lTypeFile "acadiso.lin") ; A-TEST
	; (setq lTypesList
		; (list
			; "JIS_02_4.0"
			; "JIS_08_11"
			; "JIS_08_25"
			; "JIS_09_15"
			; "JIS_09_29"
		; )
	; ) ; A-TEST
	
	(foreach x lTypesList
		(if (null (tblsearch "Ltype" x))
			(MT-importLine x lTypeFile)
		)
	) ; import linetypes

	(setq layersConfig "MT_layers.cfg")
	(if (findfile layersConfig)
		(progn
			(setq sectionName "common_default")
			(setq layersDefaultList (MT-getConfigList layersConfig sectionName))
			(setq sectionName "common")
			(setq layersConfigTmpList (MT-getConfigList layersConfig sectionName))
			(foreach x layersDefaultList
				(setq layersConfigList
					(append layersConfigList
						(list (assoc (cdr x) layersConfigTmpList))
					)
				)
			)
		)
	)
	(if (= layersConfigList nil)
		(progn
			(setq layersList
				(list
					(list "_025_common" "0.25" "7" "Continuous" "_plot")
					(list "_Dimensions" "0.18" "254" "Continuous" "_plot")
					(list "_Text" "0.25" "7" "Continuous" "_plot")
					(list "_XREF" "0.25" "7" "Continuous" "_plot")
					(list "_Vport" "0.00" "1" "Continuous" "_no")
					(list "_noPrint" "0.00" "231" "Continuous" "_no")
				)
			) ; ������� ������ ������ �� ����� (�������� ��������� cfg-������ MT_main � MT_layers)
			(setq dimensionLayer "_Dimensions"); ������ � cfg-�����
		)
		(progn
			(setq layerPropertyList
				(MT-stringToList 
					(cdr 
						(assoc "_dimLay" layersConfigList)
					)
					","
				)
			)
			(setq dimensionLayer 
				(car layerPropertyList)
			)
			(foreach x layersConfigList
				(setq layerPropertyList
					(MT-stringToList 
						(cdr x)
						","
					)
				)
				(setq layersList (append layersList (list layerPropertyList)))
			)
		)
	)

	(foreach x layersList
		(if (not (tblsearch "layer" (car x)))
			(vl-cmdf "_layer"
				"_set" "0"
				"_Make" (car x) 
				"_LWeight" (atof (car (cdr x))) (car x) 
				"_Color" (atoi (cadr (cdr x))) (car x) 
				"_Ltype" (caddr (cdr x)) (car x) 
				"_Plot" (cadddr (cdr x)) (car x) 
				""
			)
		)
	) ; create layers
	(setvar "clayer" "0")

	(if (>= (atof aVer) 20.1)
		(setvar "DIMLAYER" dimensionLayer); for ACAD2016+
	)
	
	(setq sectionName "fonts_default")
	(setq txtStylePropertyList (MT-getConfigList configFileName sectionName))
	(setq txtStylesList
		(list
			(list 
				"Greeks" 
				"greeks.shx" 
				(atof (cdr (assoc "_wgt" txtStylePropertyList))); 0.7
				(atof (cdr (assoc "_ang" txtStylePropertyList))); 0
			) ;  ollways  create!
			(list 
				"GOST_2.304" 
				(cdr (assoc "_font" txtStylePropertyList)) 
				(atof (cdr (assoc "_wgt" txtStylePropertyList))); 0.7
				(atof (cdr (assoc "_ang" txtStylePropertyList))); 0
			)
		)
	) ; load default fontnames
	
	(foreach x txtStylesList
		(if (not (tblobjname "STYLE" (car x)))
			(MT-createTextStyle x)
		)
		
	); create text styles
	
	(setq dimStylesList
		(list
		(list
			"MT-common" ; style name
			"GOST_2.304" ; text style name
			2.5 ; text height
			1.0 ; roundof
			0 ; precision
			Nil ; short lines (not used currently)
		)
		(list
			"MT-OGP" 
			"GOST_2.304" 
			2.0 
			0.01 
			2 
			Nil
		)
		)
	)
	
	(foreach x dimStylesList
		(if (not (tblobjname "DIMSTYLE" (car x)))
			(MT-createDimStyle x)
		)
	); create dimstyles
	
	(if (and
			(tblobjname "DIMSTYLE" (caar dimStylesList))
			(/= (getvar "DIMSTYLE") (caar dimStylesList))
			(/= (getvar "DIMSTYLE") (caadr dimStylesList))
		)
		(vl-cmdf "_-dimstyle" "_Restore" (caar dimStylesList))
	) ; set current
	
	(if (/= (getvar "cprofile") p_name0)
		(progn
			(setq sectionName "menu_default")
			(setq menuList (MT-getConfigList configFileName sectionName))
			(MT-menuUnload T)
			(MT-menuLoad menuList)
		)
	); load menu
	
	; (setq cmdList 
		; (list
			
		; )
	; )
	; (setq res (MT-cmd-load cmdList)); load commands
	
	; (if res
		; (prompt "\n������� CAD-MT ���������. ")
		; (prompt "\n������ ��������! ")
	; )
	
	(setvar "clayer" curLayer)
	(setvar "TEXTSTYLE" "GOST_2.304")
	
	(prompt "\n������� CAD-MT ���������. ")
	(setvar "cmdecho" oldEcho)
)

(defun MT-cmd-load (cmdList / x res)
; command loader
; cmdList : (commandName fileName funcName)

	(setq res T)
	(foreach x cmdList
		(if (findfile (car (cdr x)))
			(list 'defun (car x) ()
				(list 'cond
					(list 'if ()
						(list 'progn
							(list 'findfile (car (cdr x)))
							(list 'cadr (cdr x))
						)
					)
				)
			)
			(setq res nil)
		)
		
	); load commands
)
