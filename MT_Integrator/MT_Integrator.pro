#-------------------------------------------------
#
# Project created by QtCreator 2018-09-07T19:07:07
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = MT_Integrator
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

#QMAKE_EXTRA_TARGETS += before_build makefilehook

#makefilehook.target = $(MAKEFILE)
#makefilehook.depends = .beforebuild

#PRE_TARGETDEPS += .beforebuild

#before_build.target = .beforebuild
#before_build.depends = FORCE
#before_build.commands = chcp 1251

SOURCES += \
    app.cpp \
        main.cpp \
        mainwindow.cpp \
    Acad.cpp \
    acadsrv.cpp \
    countwindows.cpp \
    files.cpp \
    Reg.cpp \
    thread.cpp \
    profilesrv.cpp \
    filesACD.cpp \
    configSrv.cpp \
    filesQt.cpp

HEADERS += \
    app.h \
        mainwindow.h \
    Acad.h \
    acadsrv.h \
    countwindows.h \
    Reg.h \
    thread.h \
    files.h \
    profilesrv.h \
    filesACD.h \
    configSrv.h \
    filesQt.h

FORMS += \
        mainwindow.ui
