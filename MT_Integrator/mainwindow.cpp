#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "files.h"
#include "filesACD.h"
#include "acadsrv.h"
#include "profilesrv.h"
#include "configSrv.h"
#include "app.h"
#include <QMessageBox>
#include <QProgressDialog>
#include <QProcess>
#include <QVariant>
#include <QThread>
#include <QTime>
#include <QDir>
#include <QPalette>
#include <string>
#include <windows.h>
#include <cstdio>

#define TIME_OUT 120000// 120 sec !!

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QString listTitleStr = "Настройка";

    ui->groupBox->setTitle(listTitleStr);

    registry = new Reg::RegFunc();
    acd = new Acad::AcadApp();
    modelAcadList = new QStringListModel(this);

    ui->listView->setModel(modelAcadList);
    ui->listView->setSelectionMode(QAbstractItemView::MultiSelection);
    ui->listView->setSelectionBehavior(QAbstractItemView::SelectItems);
    ui->listView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    connect(ui->listView, &QListView::clicked, this, &MainWindow::on_acadList_activated);
    connect(ui->pushButtonCancel, &QPushButton::clicked, this, &MainWindow::slotExit);
    connect(ui->actionExit, &QAction::triggered, this, &MainWindow::slotExit);
}

MainWindow::MainWindow(QWidget *parent, bool setup) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->isSetup = setup;

    QString listTitleStr;
    if(!isSetup){
		listTitleStr = "Восстановить профили для:";
        ui->checkBoxTune->setVisible(false);
    }else{
        listTitleStr = "Настроить профили для:";
    }
    ui->groupBox->setTitle(listTitleStr);

    registry = new Reg::RegFunc();
    acd = new Acad::AcadApp();
    modelAcadList = new QStringListModel(this);

    ui->listView->setModel(modelAcadList);
    ui->listView->setSelectionMode(QAbstractItemView::MultiSelection);
    ui->listView->setSelectionBehavior(QAbstractItemView::SelectItems);
    ui->listView->setEditTriggers(QAbstractItemView::NoEditTriggers);

    connect(ui->listView, &QListView::clicked, this, &MainWindow::on_acadList_activated);
    connect(ui->pushButtonCancel, &QPushButton::clicked, this, &MainWindow::slotExit);
    connect(ui->actionExit, &QAction::triggered, this, &MainWindow::slotExit);
}

MainWindow::~MainWindow(){

    delete ui;
}

void MainWindow::SetAcadListData(){

    QStringList acadNamesList;
    for(size_t i = 0; i < acadList.size(); i++){
        acadNamesList.push_back(QString::fromStdWString(acadList[i].GetProductName()));
    }
    modelAcadList->setStringList(acadNamesList);

}

QStringList MainWindow::GetProfilesList(Acad::AcadApp  *app){

    QStringList acadProfilesList;
    vector<wstring> acadProfilesTmp = app->GetAcadProfiles();
    for(size_t i = 0; i < acadProfilesTmp.size(); i++){
        acadProfilesList.push_back(QString::fromStdWString(acadProfilesTmp[i]));
    }

    return acadProfilesList;
}

void MainWindow::pushButtonOkEnabled(){
    ui->pushButtonOk->setEnabled(acadSelectedList.length() > 0);

}

QStringList MainWindow::GetFileList(QString dirPath, QString mask){

    QDir dir(dirPath);
    QStringList maskList = mask.split(" ");
    QStringList filesList = dir.entryList(maskList, QDir::Files);

    return filesList;
}

QStringList MainWindow::GetAcadTemplatesList(Acad::AcadApp  *app, int profileIndex){

    wstring keyName = L"TemplatePath";
    vector<wstring> keyList = {
        keyName
    };
    QStringList acadTemplatesList;
    vector<wstring> acadProfiles = app->GetAcadProfiles();
    vector<pair<wstring, wstring>> profilePropertyesList = app->GetProfileProperties(acadProfiles[profileIndex], keyList);
    for(size_t i = 0; i < profilePropertyesList.size(); i++){
        if (profilePropertyesList[i].first == keyName){
            wstring sAcadTemplateDir = profilePropertyesList[i].second;
            QString acadTemplateDir = QString::fromStdWString(sAcadTemplateDir);
            acadTemplatesList = GetFileList(acadTemplateDir, "*.dwt");
        }
    }

    return acadTemplatesList;
}

void MainWindow::on_pushButtonOk_clicked(){

    SetupInACD(isSetup, acadSelectedList);
    bool report;
    QString strTmp;
    if(isSetup){
        report = SaveAcdList();
        strTmp = "установки";
    }else{
        strTmp = "удаления";
        report = ClearAcdList();
    }

    bool res = true;
    QString reportStr;
    QList<Acad::AcadApp> clearAppList;

    for (int i = 0; i < reportList.size(); i++){
        if (!reportList[i].second){
            QString str = QString::fromStdWString(reportList[i].first.GetProductName());
            reportStr = reportStr + str + " - Ошибка " + strTmp +"\n";
            clearAppList.push_back(reportList[i].first);
            res = false;
        }else{

        }
    }
    if(isSetup && (!clearAppList.empty())){
        SetupInACD(false, clearAppList);
    }
    if(res){
        reportStr = "Успешно завершено\n";
    }
    if (!report){
        reportStr = reportStr+ "Ошибка сохранения настроек";
    }

    QMessageBox msgBox;
    msgBox.setText(reportStr);
    msgBox.setWindowTitle("MT-Integrator");

    msgBox.exec();

    if(isSetup && (ui->checkBoxTune->isChecked())){
        StartTuning();
    }

    this->close();
}

void MainWindow::on_acadList_activated()
{
    acadSelectedList.clear();
    QModelIndexList indexList = ui->listView->selectionModel()->selectedIndexes();
    for (int i = 0; i < indexList.count(); i++){
        int tmp = indexList.at(i).row();
        acadSelectedList.append(acadList[tmp]);
    }
    pushButtonOkEnabled();
}

void MainWindow::SetupInACD(bool isSetup, QList<Acad::AcadApp>  appList){

    bool isSilent = true; // Hide/Show ACAD window (false in debug)

    vector<wstring> keyList = {
        L"TemplatePath"
    };

    QString scriptFileName = QDir::tempPath() + "/" + "setup.scr"; // script in %tempdir%
    bool setupScriptSucsess = Files::FilesACD::CreateSetupScriptAcd(isSetup,
                                                         scriptFileName.toStdWString(),
                                                         isSilent); // create Script

    if(setupScriptSucsess){
        for(int i= 0; i < appList.length(); i++){
            acd = &appList[i];
            QStringList profilesList = GetProfilesList(acd);
            QString profile = profilesList.front();

            vector<pair<wstring, wstring>> profilePropertyesList = acd->GetProfileProperties(profile.toStdWString(), keyList);

            QString temlateDirName = QString::fromStdWString(profilePropertyesList.back().second);
            QDir *temlateDir = new QDir(temlateDirName);
            QStringList filters;
            filters.append("*.dwt");
            QStringList templateList = temlateDir->entryList(filters, QDir::Files, QDir::Name);
            QString templateName = "acadiso.dwt";
            QString fileName;
            int pos = templateList.indexOf(templateName);
            if(pos != -1){
                fileName = temlateDir->absolutePath() + "/" + templateList[pos];
            }else{
                fileName = temlateDir->absolutePath() + "/" + templateList.back();
            }
            QStringList fileNameList = {fileName};

            bool secureModeChanged = false;
            if (acd->IsSecureModeOn(profile.toStdWString())){
                secureModeChanged = acd->SecureModeChange(profile.toStdWString());
            }// SECURELOAD disable in ACAD 2014+

            // hide acad warnings
            wstring key = L"ProfileStorage";
            wstring keyPath = acd->GetRegKeyPath() + L"\\FixedProfile\\General";
            wstring value = Reg::RegFunc::GetParametr(HKEY_CURRENT_USER, keyPath, key);
            wstring changeFileNameBase = Files::Files::ChangeSubstr(this->appPath, L"\\\\", L"\\");
            changeFileNameBase = changeFileNameBase + L"\\MiniTools\\Support\\FixedProfile_";

            bool isFixedProfileChanged;
            QString strMsg;
            if(isSetup){
                bool backupExist = Acad::ProfileSrv::CreateFixedProfileBackup(value);
                if(backupExist){
                    isFixedProfileChanged = Acad::ProfileSrv::ChangeFixedProfile(value, changeFileNameBase, acd->GetLocaleId());
                }
                strMsg = "Настройка профилей";
            }else{
                strMsg = "Восстановление профилей";
            }

            // setup
            // prolonged operation. Runing the ACAD app & Lisp programm in this
            StartAppInThread(fileNameList, scriptFileName, profile, "", strMsg, TIME_OUT, isSilent);

            if (secureModeChanged == true){
                acd->SecureModeChange(profile.toStdWString());
            }

            // restore defaults
            if(!isSetup){
                isFixedProfileChanged = Acad::ProfileSrv::RestoreFixedProfile(value);
                if(isFixedProfileChanged == true){
                    Acad::ProfileSrv::DeleteFixedProfileBackup(value);
                }
            }
        }

        bool isScriptDeleting = Files::Files::DeleteFileNow(scriptFileName.toStdWString());// delete script
        if (isScriptDeleting == false){
            // to do ..
        }

    }
    else{
        QString str = "Install script not created!";
        QMessageBox msgBox;
        msgBox.setText(str);
        msgBox.setWindowTitle("Error!");

        msgBox.exec();
    }

}

void MainWindow::StartAppInThread(QStringList fileNameList,
                                  QString scriptFileName,
                                  QString profile,
                                  QString path,
                                  QString text,
                                  int timeOut,
                                  bool isSilent){

    vector<wstring> keyList = {
        L"ACAD"
    };
    wstring wsAppName = Acad::AcadSrv::GetAcadFullName(acd);
    QString appName = QString::fromStdWString(wsAppName);

//    vector<pair<wstring, wstring>> profilePropertyesList =
//            acd->GetProfileProperties(profile.toStdWString(), keyList);
//    QString supportPath = QString::fromStdWString(profilePropertyesList.front().second);
//    if(!path.isEmpty()){
//        supportPath = supportPath  + path;
//    }

    QString supportPath = path; // for setup - ""!

    // progressbar color
    QPalette plt = this->palette();
    plt.setColor(QPalette::Highlight, QColor(77, 166, 255, 255));

    threadObject = new Ui::Thread;
    threadObject->SetArg(scriptFileName, fileNameList, appName, profile, supportPath, timeOut, isSilent);
    connect(threadObject, &Ui::Thread::reportResult, this, &MainWindow::AppReport);

    threadObject->start();
    appRuning = true;

    // progressbar window
    QProgressDialog *pprd = new QProgressDialog();
    pprd->setWindowFlags(Qt::WindowTitleHint | Qt::CustomizeWindowHint);
    pprd->setCancelButton(0);
    pprd->setWindowTitle(text);
    pprd->setLabelText(QString::fromStdWString(acd->GetProductName()));
    pprd->setMinimum(0);
    pprd->setMinimumDuration(0);
    pprd->setModal(true);
    pprd->setAutoReset(false);
    pprd->setAutoClose(true);
    pprd->setPalette(plt);

    QTime time;
    time.start();
    int i;
    pprd->setMaximum(timeOut);
    while (appRuning){
        i = time.elapsed();
        pprd->setValue(i);
        qApp->processEvents();
        if(pprd->wasCanceled()){
            emit workCanceled();
            break;
        }// next setup was canceled!
    }

    pprd->setValue(pprd->maximum());
    threadObject->wait();

    delete pprd;
    delete threadObject;

}

void MainWindow::AppReport(int value){

    pair<Acad::AcadApp, bool> tmp;
    tmp.first = *acd;
    if(value == 0){
        tmp.second = true;
    }else{
        tmp.second = false;
    }
    reportList.push_back(tmp);
    appRuning = false;
}

void MainWindow::slotExit(){

    this->close();
    // delete tmp files and close window
}

void MainWindow::on_actionAbout_triggered()
{
    QString str;
    wstring fileName = this->appPath + L"\\MiniTools\\Main\\MT_about.txt";
    list<wstring> stringListTmp = Files::Files::GetFileLinesList(fileName);
    QStringList stringList;
    for(list<wstring>::iterator it = stringListTmp.begin(); it != stringListTmp.end(); it++){
        stringList.push_back(QString::fromStdWString(*it));
    }
    if (stringList.empty()){
        str = "Ошибка открытия файла";
    } else {
        while (!stringList.empty()){
            str = str + stringList.front() + "\n";
            stringList.pop_front();
        }
    }

    QMessageBox::about(0, "About", str);
}

void MainWindow::StartTuning(){
    // start tune app

    wstring appNameTmp = this->appPath + L"\\MT_Configurator.exe";
    QString appName = QString::fromStdWString(appNameTmp);
    bool appFileExists = QFile(appName).exists();
    if(appFileExists){
        QProcess::startDetached(appName);
    }else{
        QString str = "Приложение не найдено!";
        QMessageBox msgBox;
        msgBox.setText(str);
        msgBox.setWindowTitle("Error!");

        msgBox.exec();
    }
}

bool MainWindow::SaveAcdList(){

    map<wstring, wstring> paramsSource;
    vector<wstring> paramsNames = {
        L"_acad",
        L"_path"
    };

    wstring appPath = this->appPath;
    appPath = Files::Files::ChangeSlash(appPath);
    paramsSource.insert(pair<wstring, wstring>(L"_path", appPath));

    wstring fileName = appPath + L"\\MiniTools\\Main\\MiniTools.cfg";
    wstring sectName = L"main";

    list<wstring> sourceList = Files::Files::GetFileLinesList(fileName);
    map<wstring, wstring> paramsTarget = Files::ConfigSrv::GetSectParams(fileName, sectName);

    list<wstring> acadAppList;
    for(int i = 0; i < reportList.size(); i++){
        if(reportList[i].second == true){
            wstring str = reportList[i].first.GetProductName();
            acadAppList.push_back(str);
        }
    }

    wstring acadStrTmp;
    wstring sep = L",";

    while(!acadAppList.empty()){
        wstring tmpStr = acadAppList.front();
        wstring tmpValue = L"";
        map<wstring, wstring>::iterator it;
        it = paramsTarget.find(L"_acad");
        if(it != paramsTarget.end()){
            tmpValue = it->second;
        }
        size_t pos = tmpValue.find(tmpStr);
        if(pos == std::string::npos){
            if(!acadStrTmp.empty()){
                acadStrTmp = acadStrTmp + sep;
            }
            acadStrTmp = acadStrTmp + acadAppList.front();
        }
        acadAppList.pop_front();
    }
    paramsSource.insert(pair<wstring, wstring>(L"_acad", acadStrTmp));

    for (int i = 0; i < paramsNames.size(); i++){
        map<wstring, wstring>::iterator itTarget = paramsTarget.find(paramsNames[i]);
        map<wstring, wstring>::iterator itSource = paramsSource.find(paramsNames[i]);
        if(itSource != paramsSource.end()){
            wstring valueSource = itSource->second;
            wstring valueTarget;
            if(itTarget != paramsTarget.end()){
                valueTarget = itTarget->second;
            }else{
                paramsTarget.insert(pair<wstring, wstring>(paramsNames[i], L""));
            }
            if((valueTarget == L"") || (paramsNames[i] != L"_acad")){
                paramsTarget.at(paramsNames[i]) = valueSource;
            }else{
                valueTarget = valueTarget + sep + valueSource;
                paramsTarget.at(paramsNames[i]) = valueTarget;
            }
        }
    }

    list<wstring> lineList = Files::ConfigSrv::InsertParamsInSect(sourceList, paramsTarget, sectName);
    bool res = Files::Files::CreateTextFile(fileName, lineList);

    return res;
}

bool MainWindow::ClearAcdList(){

    wstring fileName = this->appPath + L"\\MiniTools\\Main\\MiniTools.cfg";
    wstring sectName = L"main";

    list<wstring> sourceList = Files::Files::GetFileLinesList(fileName);
    map<wstring, wstring> paramsTarget = Files::ConfigSrv::GetSectParams(fileName, sectName);

    list<wstring> acadAppList;
    for(int i = 0; i < reportList.size(); i++){
        if(reportList[i].second == true){
            wstring str = reportList[i].first.GetProductName();
            acadAppList.push_back(str);
        }
    }

    wstring sep = L",";
    wstring tmpValue = L"";
    map<wstring, wstring>::iterator it;
    it = paramsTarget.find(L"_acad");
    size_t pos;
    if(it != paramsTarget.end()){
        tmpValue = it->second;
        while(!acadAppList.empty()){
            wstring tmpStr = acadAppList.front();
            pos = tmpValue.find(tmpStr);
            while(pos != std::string::npos){
                size_t posSep = tmpValue.find(sep, pos);
                wstring tmpValueSubstr = tmpValue.substr(pos, posSep - pos);
                if(tmpValueSubstr.back() == sep.operator [](0)){
                    tmpValueSubstr.pop_back();
                }
                if(tmpValueSubstr == tmpStr){
                    tmpValue.erase(pos, pos + tmpStr.length());
                }
                pos = tmpValue.find(tmpStr, posSep);
            }
            acadAppList.pop_front();
        }
        while((pos = tmpValue.find(sep + sep)) != std::string::npos){
            tmpValue.erase(pos, 1);
        }
        if(!tmpValue.empty()){
            if(tmpValue.back() == sep.operator [](0)){
                tmpValue.pop_back();
            }
            if(tmpValue.front() == sep.operator [](0)){
                tmpValue = tmpValue.substr(1);
            }
        }
    }
    paramsTarget.at(L"_acad") = tmpValue;

    list<wstring> lineList = Files::ConfigSrv::InsertParamsInSect(sourceList, paramsTarget, sectName);
    bool res = Files::Files::CreateTextFile(fileName, lineList);

    return res;
}
