/*
 * based on
 * https://blogs.msdn.microsoft.com/oldnewthing/20140127-00/?p=1963/
*/

#ifndef COUNTWINDOWS_H
#define COUNTWINDOWS_H

#include <windows.h>
#include <vector>

namespace Acad
{
    struct Proc {
        HWND handle;
        DWORD id;
    };

    class CountWindows{
    public:
        Acad::Proc process;

        CountWindows();
        ~CountWindows();
        std::vector<Acad::Proc> CountThem();

    private:
        static BOOL CALLBACK StaticWndEnumProc(HWND hwnd, LPARAM lParam);
        BOOL WndEnumProc(HWND hwnd);

        std::vector<Acad::Proc> processVec;
    };

}

#endif // COUNTWINDOWS_H
