#include "mainwindow.h"
#include <QApplication>
#include <QMessageBox>
#include <QStyleFactory>
#include <algorithm>
#include "acadsrv.h"
#include "configSrv.h"
#include "files.h"
#include "app.h"

#pragma comment(lib, "Advapi32.lib")
#pragma comment(lib, "User32.lib")

int main(int argc, char *argv[])
{
    bool isSetup = true;
    if (argc > 1){
        string arg = argv[1];
        if(arg == "-remove"){
            isSetup = false;
        }
    }

    int result = 0;
    vector<Acad::AcadApp> acadListTmp = Acad::AcadSrv::GetInstalledAcad();

    // remove acad 2013 from list
    vector<Acad::AcadApp> acadList;
    for(size_t i = 0; i < acadListTmp.size(); i++){
        Acad::AcadApp item = acadListTmp[i];
        double version = stod(item.GetRelease().substr(0,4), 0);
        if(version >= 19.1 || (version >= 18.0 && version < 19.0)){
            acadList.push_back(item);
        }
    }

    // get current path ()
    wstring currentPath = App::App::GetAppPath();

    if(!isSetup){
        wstring fileName = currentPath + L"\\MiniTools\\Main\\MiniTools.cfg";
        wstring sectName = L"main";
        wstring paramStr = L"";
        wstring key = L"_acad";
        map<wstring, wstring> params = Files::ConfigSrv::GetSectParams(fileName, sectName);
        map<wstring, wstring>::iterator it1 = params.find(key);
        wstring sep = L",";
        if(it1 != params.end()){
            paramStr = it1->second;
        }
        vector<Acad::AcadApp>::iterator i = acadList.begin();
        while(i != acadList.end()){
            wstring str = i->GetProductName();
            size_t pos = paramStr.find(str);
            if(pos == std::string::npos){
                acadList.erase(i);
                i = acadList.begin();
            }else{
                bool clear = false;
                while(pos != std::string::npos){
                    size_t posSep = paramStr.find(sep, pos);
                    wstring paramStrTmp = paramStr.substr(pos, posSep - pos);
                    if(paramStrTmp != str){
                        clear = true;
                        pos = std::string::npos;
                    }else{
                        pos = paramStr.find(str, posSep);
                    }
                }
                if (clear){
                    acadList.erase(i);
                    i = acadList.begin();
                }else{
                    i++;
                }
            }
        }
    }

    QApplication app(argc, argv); /// !!
    app.setStyle(QStyleFactory::create("Fusion"));

    if (acadList.empty()){
        QString message = "ACAD application not found!";
        QMessageBox msgBox;
        msgBox.setText(message);
        msgBox.setWindowTitle("Error!");
        msgBox.exec();
    }else{
        MainWindow w(0, isSetup);
        w.appPath = currentPath;
        w.acadList = acadList;
        w.SetAcadListData();
        w.show();
        result = app.exec();
    }

    return result;
}
