#ifndef SHELL_H
#define SHELL_H

#include <QProcess>
#include <QString>

namespace Ui{
    class Shell;
}

class Shell
{
public:
    Shell();
    Shell(QString name, QStringList args);
    ~Shell();

private:
    QProcess *process;
    QString programmName;
    QStringList arguments;

};

#endif // SHELL_H
