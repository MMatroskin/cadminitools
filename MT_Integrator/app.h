#ifndef APP_H
#define APP_H
#include <string>

using namespace std;

namespace App
{
    class App{
    public:
        App();
        ~App();

        static wstring GetAppPath();
    };
}

#endif // APP_H
