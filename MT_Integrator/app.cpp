#include "app.h"
#include <windows.h>

namespace App{
    App::App()
    {}
    App::~App()
    {}

    wstring App::GetAppPath(){
        wstring currentPath = L"";
        WCHAR buffer[MAX_PATH];
        DWORD r = GetModuleFileName(NULL, buffer, sizeof(buffer) / sizeof(buffer[0]));
        if(r > 0){
            currentPath = buffer;
            size_t p = currentPath.find_last_of(L"\\");
            currentPath = currentPath.substr(0, p);
        }else{
            wchar_t path[MAX_PATH];
            GetCurrentDirectory(sizeof(path), path);
            currentPath = path; // path of parent app !!
        }
        return currentPath;
    }
}

