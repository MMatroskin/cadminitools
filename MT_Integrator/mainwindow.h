#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "ui_mainwindow.h"
#include <QMainWindow>
#include <QStringListModel>
#include <QString>
#include <QList>
#include <QDir>
#include <utility>
#include <vector>
#include <list>
#include "Reg.h"
#include "Acad.h"
#include "thread.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow, public Ui_MainWindow
{
    Q_OBJECT

public:
    Reg::RegFunc *registry;
    vector<Acad::AcadApp>  acadList;
    bool isSetup;
    wstring appPath;

    explicit MainWindow(QWidget *parent = 0);
    explicit MainWindow(QWidget *parent = 0, bool setup = true);
    ~MainWindow();

    void SetAcadListData();

signals:
    void workCanceled();

public slots:
    void AppReport(int value);

private slots:
    void on_pushButtonOk_clicked();
    void pushButtonOkEnabled();
    void slotExit();
    void on_actionAbout_triggered();
    void on_acadList_activated();

private:
    Ui::MainWindow *ui;
    QStringListModel *modelAcadList;
    Acad::AcadApp *acd;
    QList<Acad::AcadApp>  acadSelectedList;
    Ui::Thread *threadObject;
    bool appRuning;
    vector<pair<Acad::AcadApp, bool>> reportList;

    QStringList GetFileList(QString dirPath, QString mask);
    QStringList GetProfilesList(Acad::AcadApp  *app);
    QStringList GetAcadTemplatesList(Acad::AcadApp  *app, int profileIndex);
    void SetupInACD(bool isSetup, QList<Acad::AcadApp> appList);
    void StartAppInThread(QStringList fileNameList,
            QString scriptFileName,
            QString profile,
            QString path,
            QString text,
            int timeOut,
            bool isSilent);
    void StartTuning();
    bool SaveAcdList();
    bool ClearAcdList();

};

#endif // MAINWINDOW_H
